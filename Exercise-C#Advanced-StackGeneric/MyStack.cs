﻿

using System.Text;

namespace Exercise_C_Avanced_StackGeneric
{
    public class MyStack<T>
    {
        public T[] items { get; set; }
        int top = -1;

        public MyStack(int maxStack)
        {
            items = new T[maxStack];
        }

        public T Push(T item)
        {
            top++;
            if (isFull())
            {
                top--;
                throw new Exception("Stack is full");
            }
            items[top] = item;
            return item;
        }

        public T Pop()
        {
            if (isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            var topAtValue = items[top];
            top--;
            return topAtValue;
        }

        public T Top()
        {
            if (isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            return items[top];

        }
        public int LastItemIndex()
        {
            if (isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            return top;
        }

        public void Print()
        {
            for (int i = 0; i <= top; i++)
            {
                Console.WriteLine("Number of Stack: " + items[i]);
            }
        }

        public bool isEmpty()
        {
            return top == -1;
        }
        public bool isFull()
        {
            return top == items.Length;
        }
        public int Count()
        {
            return top + 1;
        }
    }
}
