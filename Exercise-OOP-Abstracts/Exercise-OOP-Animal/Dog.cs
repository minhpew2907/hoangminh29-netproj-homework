﻿

namespace Animal
{
    public class Dog : Animals
    {
        public Dog(string name, int age, string type, string breed) : base(name, age, type)
        {
            _Name = name;
            _Age = age;
            _Type = type;
            _Breed = breed;
        }

        public string _Breed { get; set; } 

        public void Bark()
        {
            Console.WriteLine("Woof! Woof!");
        }
    }
}
