﻿

namespace Exercise_C_Acvanced_Delegate
{
    public class SignatureManager : DelegateOperation
    {
        protected override int PerformOperation(int a, int b, MathOperation mathOperation)
        {
            return base.PerformOperation(a, b, mathOperation);
        }
        private int Addition(int x, int y) => x + y;
        private int Subtraction(int x, int y) => x - y;
        private int Multiplication(int x, int y) => x * y;
        private int Division(int x, int y) => x / y;
        private int ChiaLayDu(int x, int y) => x % y;
        private int x;
        private int y;

        public void Input()
        {
            Console.WriteLine("Enter your x: ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter your y: ");
            y = int.Parse(Console.ReadLine());
        }

        public void AdditionNumber()
        {
            Input();
            var addition = new MathOperation(Addition);
            var result = PerformOperation(x, y, addition);
            Console.WriteLine(result);
        }

        public void SubtractionNumber()
        {
            Input();
            var subtraction = new MathOperation(Subtraction);
            var result = PerformOperation(x, y, subtraction);
            Console.WriteLine(result);
        }

        public void MultiplicationNumber()
        {
            Input();
            var multiplication = new MathOperation(Multiplication);
            var result = PerformOperation(x, y, multiplication);
            Console.WriteLine(result);
        }

        public void DivisionNumber()
        {
            Input();
            var division = new MathOperation(Division);
            var result = PerformOperation(x, y, division);
            Console.WriteLine(result);
        }

        public void ChiaLayDuNumber()
        {
            Input();
            var chialaydu = new MathOperation(ChiaLayDu);
            var result = PerformOperation(x, y, chialaydu);
            Console.WriteLine(result);
        }
    }
}
