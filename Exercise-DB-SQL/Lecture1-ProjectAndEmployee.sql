use master
go
drop database if exists LibraryManagements
go
create database LibraryManagements
go
use LibraryManagements
go

create table Books
(
	ID int primary key identity(1, 1),
	Title varchar(255),
	Author varchar(255),
	YearPublished date
);

create table Reader
(
	ReaderID int,
	Address varchar(255),
	PhoneNumber varchar(20),
	foreign key (ReaderID) references Books(ID)
);