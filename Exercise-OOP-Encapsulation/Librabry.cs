﻿


using System.Runtime.CompilerServices;
using System.Text;

namespace Exercise_Encapsulation
{
    public class Librabry
    {
        Book[] books;
        int maxBook;
        int top = -1;

        public Librabry(int maxBook)
        {
            this.maxBook = maxBook;
            books = new Book[maxBook];
        }

        public void AddBook(Book book)
        {
            top++;
            books[top] = book;
        }

        public void CheckOut(int id)
        {
            bool isCheckOut = false;
            for (int i = 0; i < top + 1; i++)
            {
                if (books[i]._Available is true && books[i]._Id == id)
                {
                    Console.WriteLine("Book has rented");
                    books[i]._Available = false;
                    isCheckOut = true;
                }
            }
            if (isCheckOut is false)
            {
                Console.WriteLine("Can not found");
            }
        }

        public void ReturnBook(int id)
        {
            bool isReturn = false;
            for (int i = 0; i < top + 1; i++)
            {
                if (books[i]._Available is false && books[i]._Id == id)  
                {
                    Console.WriteLine("Book has take back");
                    books[i]._Available = true;
                    isReturn = true;
                }
            }
            if(isReturn is false)
            {
                Console.WriteLine("Can not found");
            }
        }

        public string PrintBookInfo()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= top; i++)
            {
                if (books[i]._Available is true)
                {
                    sb.Append($"\n{books[i]._Id} | {books[i]._Title} | {books[i]._Author}");
                }
            }
            return sb.ToString();
        }

        public void Menu()
        {
            Console.WriteLine("---------Librabry-----------");
            Console.WriteLine("1. Add a new book ");
            Console.WriteLine("2. View list of books ");
            Console.WriteLine("3. Rent a book by id ");
            Console.WriteLine("4. Take back a book by id ");
            Console.WriteLine("5. Exit.                   ");
            Console.WriteLine("----------------------------");
        }
    }
}
