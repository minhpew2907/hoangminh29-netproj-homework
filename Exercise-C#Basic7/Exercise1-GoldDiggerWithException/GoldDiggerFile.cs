﻿class GoldDiggerFile
{
    const string FILE_PATH = @"C:\Users\ADMIN\Desktop\NET j Pro ram\Exercise-C#BasicFullCourse\Exercise-C#Basic7\Exercise1-GoldDiggerWithException\gold.txt";

    public bool ExistFile()
    {
        if (File.Exists(FILE_PATH))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void ReadFile()
    {
        if(ExistFile())
        {
            string fileContent = File.ReadAllText(FILE_PATH);
            Console.WriteLine(fileContent);
        }
        else
        {
            throw new Exception("File not exist");
        }
    }
    public void WriteFile()
    {
        string playerName = Game.playerName;
      
        if(ExistFile())
        {
            string content = "Player 1 name: " + playerName;
            File.WriteAllText(FILE_PATH, content);
        }
    }
}