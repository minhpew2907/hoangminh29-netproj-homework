﻿Employee employee = new Employee();

int choice = 0;
do
{
    employee.Exist();
    Console.WriteLine(" ------Employee Management------ ");
    Console.WriteLine("| 1. View List Of Employee.      |");
    Console.WriteLine("| 2. Add New Employee.           |");
    Console.WriteLine("| 3. Delete Employees.           |");
    Console.WriteLine("| 4. Exit.                       |");
    Console.WriteLine(" -------------------------------- ");
    Console.WriteLine("\nEnter your choice: ");
    choice = int.Parse(Console.ReadLine());
    
    switch (choice)
    {
        case 1:
            employee.PrintEmployees();
            break;
        case 2:
            employee.AddEmployee();
            break;
        case 3:
            Console.Write("Enter name to delete: ");
            string name = Console.ReadLine();
            employee.RemoveEmployee(name);
            break;
        default:
            Console.WriteLine("See You Again!");
            break;
    }
} while (choice != 4);

