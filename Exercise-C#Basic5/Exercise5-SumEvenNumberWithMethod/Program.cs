﻿int[] Input()
{
    Console.WriteLine("Enter number: ");
    int n = int.Parse(Console.ReadLine());
    int[] arrNumber = new int[n];

    for (int i = 0; i < arrNumber.Length; i++)
    {
        Console.WriteLine("Enter {0} value = ", i + 1);
        arrNumber[i] = int.Parse(Console.ReadLine());
    }
    return arrNumber;
}

int SumEvenNumbers()
{
    int[] arrNumber = Input();
    int sum = 0;
    for (int j = 0;j < arrNumber.Length; j++)
    {
        if (arrNumber[j] % 2 == 0)
        {
            sum += arrNumber[j];
        }
    }
    return sum;
}
int result = SumEvenNumbers();
Console.WriteLine("Sum list of array = " + result);