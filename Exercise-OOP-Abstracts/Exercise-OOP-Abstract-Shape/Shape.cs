﻿

namespace OOP_Abstract_Shape
{
    public abstract class Shape
    {
        public abstract double CalculateArea();
        public abstract double CalculatePerimeter();
    }
}
