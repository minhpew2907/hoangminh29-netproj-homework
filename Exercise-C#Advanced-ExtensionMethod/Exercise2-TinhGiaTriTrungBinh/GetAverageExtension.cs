﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise2_TinhGiaTriTrungBinh
{
    public static class GetAverageExtension
    {
        public static double GetAverage(this List<int> listAverage)
        {
            int sum = 0;
            for (int i = 0; i < listAverage.Count; i++)
            {
                sum += listAverage[i] / 3;
            }
            return sum;
        }
    }
}
