﻿

using System.Security.Cryptography.X509Certificates;

namespace OOP_Abstract_Shape
{
    public class Rectangle : Shape
    {
        private double _length;
        private double _width;

        public Rectangle(double length, double width)
        {
            _length = length;
            _width = width;
        }

        public override double CalculateArea()
        {
            return _length * _width;
        }

        public override double CalculatePerimeter()
        {
            return (_length + _width) * 2;
        }
    }
}
