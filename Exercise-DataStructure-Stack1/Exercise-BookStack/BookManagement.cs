﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_BookStack
{
    public class BookManagement
    {
        Book[] books;
        int maxStack;
        int top = -1;
        

        public BookManagement(int maxStack)
        {
            this.maxStack = maxStack;
            books = new Book[maxStack];
        }
        public void AddBook(string tittle, string author)
        {
            if (isFull())
            {
                throw new Exception("Stack is Full");
            }
            else
            {
                top++;
                books[top] = new Book();
                books[top].Title = tittle;
                books[top].Author = author;
            }
        }
        public string PrintBookAtTop()
        {
            if (isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            else
            {
                string bookTittle = books[top].Title;
                string bookAuthor = books[top].Author;
                return $"Tittle: {bookTittle} Author: {bookAuthor}";
            }
        }
        public string RemoveBookAtTop()
        {
           if(isEmpty())
            {
                throw new Exception("Stack is empty");
            }
            else
            {
                string bookTittle = books[top].Title;
                string bookAuthor = books[top].Author;
                top--;
                return $"Tittle: {bookTittle} Author: {bookAuthor}";
            }
        }
        public int CountElementInStack()
        {
            return top + 1;
        }
        public bool isFull()
        {
            return top == maxStack;
        }
        public bool isEmpty()
        {
            return top == -1;
        }
    }
}
