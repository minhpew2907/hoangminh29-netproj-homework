﻿using System.Data;

int choice = 0;
do
{
    Console.WriteLine("$-$-$-$-$-$-$-Chu 10 Menu-$-$-$-$-$-$-$-$-$");
    Console.WriteLine("$                                         $");
    Console.WriteLine("$ 1. Calculate Sum Of 2 number.           $");
    Console.WriteLine("$ 2. Calculate Lap Phuong Of 1 number.    $");
    Console.WriteLine("$ 3. Exit.                                $");
    Console.WriteLine("$                                         $");
    Console.WriteLine("$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$-$");
    Console.WriteLine("\nEnter your choice: ");

    choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            Console.WriteLine("Enter two number to sum up: ");
            Console.WriteLine("\nEnter first number: ");
            int firstNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter second number: ");
            int secondNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Sum of 2 number = " + (firstNumber + secondNumber));
            break;
        case 2:
            Console.WriteLine("Enter your number to solve Lap Phuong: ");
            int lapPhuongNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Lap phuong cua so " + lapPhuongNumber + " la: " + Math.Pow(lapPhuongNumber, 3));
            break;
        case 3:
            Console.WriteLine("Thank You! And see you next time");
            break;
        default:
            Console.WriteLine("Please enter again! ");
            break;
    }
} while (choice != 3);





