﻿int[] Input()
{
    Console.WriteLine("Enter length of number: ");
    int n;
    n = int.Parse(Console.ReadLine());

    int[] arrNumber = new int[n];

    for (int i = 0; i < arrNumber.Length; i++)
    {
        Console.WriteLine("Enter {0} value = ", i + 1);
        arrNumber[i] = int.Parse(Console.ReadLine());
    }
    return arrNumber;
}

int FindLargestNumber()
{
    int[] arrNumber = Input();
    int largestNumber = arrNumber[0];
    for (int j = 1; j < arrNumber.Length; j++)
    {
        if(largestNumber < arrNumber[j])
        {
            largestNumber = arrNumber[j];
        }
    }
    return largestNumber;
}
int result = FindLargestNumber(); 
Console.WriteLine("Largest Number is: " + result);
 