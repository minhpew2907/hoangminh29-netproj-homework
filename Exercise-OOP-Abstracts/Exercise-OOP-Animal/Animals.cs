﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    public class Animals
    {
        public string _Name { get; set; }
        public int _Age { get; set; }
        public string _Type { get; set; }

        public Animals(string name, int age, string type)
        {
            _Name = name;
            _Age = age;
            _Type = type;
        }

        public void Speak()
        {
            Console.WriteLine($"The {_Type} named {_Name} says");
        }
    }
}
