﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Exercise_EFCore_EmployeeManagement
{
    public class StudentManagement
    {
        private AppDbContext context;

        public StudentManagement()
        {
            context = new AppDbContext();
        }
        public void AddStudent(Student student)
        {
            context.Students.Add(student);
            Console.WriteLine("Student has been added!");
            context.SaveChanges();
        }

        public string ViewStudentList()
        {
            StringBuilder sb = new StringBuilder();
            var list = context.Students.ToList();
            foreach (var student in list)
            {
                sb.Append($"Name: {student.Name} - Age: {student.Age} - Major: {student.Major} - Gender: {student.Gender}\n");
            }
            return sb.ToString();
        }

        public void EditStudent(int id)
        {
            var studentEdit = context.Students.Find(id);
            if (studentEdit is null)
            {
                throw new Exception("Id not exist!");
            }
            Console.WriteLine("Enter new name: ");
            studentEdit.Name = Console.ReadLine();
            Console.WriteLine("Enter new age: "); 
            studentEdit.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter new major: ");
            studentEdit.Major = Console.ReadLine();
            Console.WriteLine("Enter new gender: ");
            studentEdit.Gender = Console.ReadLine();
            context.SaveChanges();
        }

        public void DeleteStudent(int id)
        {
            var student = context.Students.Find(id);
            if (student is null)
            {
                throw new Exception("Id not exist!");
            }
            context.Students.Remove(student);
            context.SaveChanges();
            Console.WriteLine("Student has been remove");
        }

        public string SearchStudent(string name)
        {
            var studentSearch = context.Students.Where(student => student.Name.Equals(name)).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in studentSearch)
            {
                stringBuilder.Append($"Name: {item.Name} - Age: {item.Age} - Major: {item.Major} - Gender: {item.Gender}");
            }
            return stringBuilder.ToString();
        }

        public void StatisticStudent()
        {
            double maleStudent = context.Students.Count(student => student.Gender.ToLower().Contains("male"));
            double femaleStudent = context.Students.Count(student => student.Gender.ToLower().Equals("female"));
            double totalStudent = maleStudent + femaleStudent;
            double percentMale = maleStudent / totalStudent * 100;
            double percentFemale = femaleStudent / totalStudent * 100;
            Console.WriteLine($"Amount of Male: {percentMale} % \n" +
                              $"Amount of Femal: {percentFemale} %");
        }
    }
}
