﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_C_Acvanced_Delegate
{
    public abstract class DelegateOperation
    {
        protected delegate int MathOperation(int a, int b);
        protected virtual int PerformOperation(int a, int b, MathOperation mathOperation)
        {
            return mathOperation(a, b);
        }
    }
}
