﻿using System.Text;

Console.OutputEncoding = Encoding.UTF8;

Console.WriteLine("Enter your number(1-5): ");
int number = int.Parse(Console.ReadLine());
switch (number)
{
    case 1:
        Console.WriteLine("Xin Chào!");
        break;
    case 2:
        Console.WriteLine("Chào buổi sáng!");
        break;
    case 3:
        Console.WriteLine("Chào buổi trưa!");
        break;
    case 4:
        Console.WriteLine("Chào buổi chiều!");
        break;
    case 5:
        Console.WriteLine("Chào buổi tối!");
        break;
    default:
        Console.WriteLine("Vui lòng chọn lại 1-5");
        break;
}