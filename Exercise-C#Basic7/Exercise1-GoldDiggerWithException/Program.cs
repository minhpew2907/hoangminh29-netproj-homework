﻿Game game = new Game();
GoldDiggerFile gdfile = new GoldDiggerFile();

int choice = 0;
do
{
    try
    {
        game.GameMenu();
        choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {
            case 1:
                game.TurnGoldToCoin();
                gdfile.WriteFile();
                break;
            case 2:
                gdfile.ReadFile();
                break;
            default:
                Console.WriteLine("See You Again!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice < 3);