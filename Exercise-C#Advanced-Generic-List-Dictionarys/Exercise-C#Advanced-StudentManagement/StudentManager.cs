﻿

using System.Timers;

namespace StudentManagment
{
    public class StudentManager
    {
        List<Student> students = new List<Student>();

        public void AddStudent(Student student)
        {
            students.Add(student);
        }

        public bool RemoveStudent(string name)
        {
            foreach (Student student in students)
            {
                if (student.Name.Equals(name))
                {
                    students.Remove(student);
                    return true;
                }
            }
            return false;
        }

        public void DisplayStudents()
        {
            foreach (Student student in students)
            {
                Console.WriteLine("Student Name: " + student.Name);
                Console.WriteLine("Student Age: " + student.Age);
                Console.WriteLine("Student GPA: " + student.Gpa);
                Console.WriteLine();
            }
        }

        public bool FindStudentByName(string name)
        {
            foreach (Student student in students)
            {
                if (student.Name.Equals(name))
                {
                    Console.WriteLine($"\nStudent name has found: \nName: {student.Name} \nAge: {student.Age} \nGpa: {student.Gpa}");
                    return true;
                }
            }
            return false;
        }
    }
}
