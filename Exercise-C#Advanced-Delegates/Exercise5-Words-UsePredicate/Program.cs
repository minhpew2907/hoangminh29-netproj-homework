﻿using System;

var words = new List<string> { "apple", "banana", "orange", "kiwi", "mango" };

Predicate<string> pre = Filters;
List<string> filteredWords = FilterWords(words, pre);

foreach (string word in filteredWords)
{
    Console.WriteLine(word);
}

List<string> FilterWords(List<string> words, Predicate<string> pre)
{
    var result = new List<string>();
    foreach (string word in words)
    {
        if (pre(word))
        {
            result.Add(word);
        }
    }
    return result;
}

bool Filters(string word)
{
    return word.Length >= 5;
}