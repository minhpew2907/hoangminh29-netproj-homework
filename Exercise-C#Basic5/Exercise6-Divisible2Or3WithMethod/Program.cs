﻿int[] Input()
{
    Console.WriteLine("Enter number: ");
    int[] arrNumber = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    return arrNumber;
}

int SumDivisibleNumbers(int[]arrNumber)
{
    int sum = 0;
    for (int i = 0; i < arrNumber.Length; i++)
    {
        if (arrNumber[i] % 2 == 0)
        {
            sum += arrNumber[i];
        }
        if (arrNumber[i] % 3 == 0)
        {
            sum += arrNumber[i];
        }
    }
    return sum;
}
int[] arrNumber = Input();
int result = SumDivisibleNumbers(arrNumber);
Console.WriteLine("Sum list of array: " + result);