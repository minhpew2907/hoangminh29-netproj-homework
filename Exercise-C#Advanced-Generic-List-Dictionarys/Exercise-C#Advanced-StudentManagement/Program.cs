﻿using StudentManagment;
using System.Text;
using System.Transactions;

StudentManager manager = new StudentManager();

int choice = 0;
do
{
    try
    {
        Console.WriteLine("------Student Management-------");
        Console.WriteLine(" 1. Add new student.           ");
        Console.WriteLine(" 2. Remove student.            ");
        Console.WriteLine(" 3. Display student list.      ");
        Console.WriteLine(" 4. Find student by name.      ");
        Console.WriteLine(" 5. Exit.                      ");
        Console.WriteLine("-------------------------------");
        Console.WriteLine("\nEnter your choice: ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                Student student = new Student();
                Console.WriteLine("Enter your name: ");
                student.Name = Console.ReadLine();

                Console.WriteLine("Enter your age: ");
                student.Age = int.Parse(Console.ReadLine());
                if (student.Age < 0)
                {
                    throw new Exception("Please enter age more than 0.");
                }

                Console.WriteLine("Enter your gpa: ");
                student.Gpa = double.Parse(Console.ReadLine());
                if (student.Gpa >= 100000)
                {
                    throw new Exception("Your gpa is out of of limit.");
                }
                else if (student.Gpa <= 0)
                {
                    throw new Exception("Your gpa much largest than 0");
                }
                manager.AddStudent(student);
                break;
            case 2:
                Console.WriteLine("Enter student name to remove: ");
                var Remove = Console.ReadLine();
                var check = manager.RemoveStudent(Remove);
                if (check is true)
                {
                    Console.WriteLine("Remove success!");
                }
                else
                {
                    Console.WriteLine("Name does not exist!");
                }
                break;
            case 3:
                Console.WriteLine("---------Student information---------");
                manager.DisplayStudents();
                break;
            case 4:
                Console.WriteLine("Enter student name: ");
                var Find = Console.ReadLine();
                var check2 = manager.FindStudentByName(Find);
                if (check2 is false)
                {
                    Console.WriteLine("Name does not exist");
                }
                break;
            case 5:
                Console.WriteLine("Good bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);

