﻿

namespace Bank_UseQueue
{
    public class Bank
    {
        public string CustomerName;
        public int MoneyWithdraw;

        public Bank()
        {

        }

        public Bank(string customerName, int moneyWithdraw)
        {
            CustomerName = customerName;
            MoneyWithdraw = moneyWithdraw;
        }
    }
}
