﻿using Microsoft.EntityFrameworkCore;


namespace Exercise_EFCore_EmployeeManagement
{
    public class AppDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connect = "Data Source=DESKTOP-O6PD73S\\MINH;Database=StudentManagement;User ID=sa;pwd=1;Trusted_Connection=True;TrustServerCertificate=True";
            optionsBuilder.UseSqlServer(connect);
        }
    }
}
