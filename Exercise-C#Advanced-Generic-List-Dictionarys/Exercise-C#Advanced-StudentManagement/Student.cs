﻿

namespace StudentManagment
{
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Gpa { get; set; }
    }
}
