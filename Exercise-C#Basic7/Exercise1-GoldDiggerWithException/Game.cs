﻿using System.Numerics;

class Game
{
    const int NUMBER_SALE_LEVEL1 = 10;
    const int NUMBER_SALE_LEVEL1_PRICE = 10;
    const int NUMBER_SALE_LEVEL2 = 5;
    const int NUMBER_SALE_LEVEL2_PRICE = 5;
    const int NUMBER_SALE_LEVEL3 = 3;
    const int NUMBER_SALE_LEVEL3_PRICE = 2;
    const int NUMBER_SALE_LEVEL_REMAIN = 1;

    public static int price10gold = 0;
    public static int price5gold = 0;
    public static int price3gold = 0;
    public static int lastPrice = 0;
    public static int goldAcess;
    public static string playerName;

    public Game()
    {

    }
    public int CheckGoldToCoin(int goldAcess)
    {
        int firstGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL1);
        price10gold = firstGold * NUMBER_SALE_LEVEL1_PRICE;
        goldAcess -= firstGold;

        int secondGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL2);
        price5gold = secondGold * NUMBER_SALE_LEVEL2_PRICE;
        goldAcess -= secondGold;

        int thirdGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL3);
        price3gold = thirdGold * NUMBER_SALE_LEVEL3_PRICE;
        goldAcess -= thirdGold;

        lastPrice = goldAcess * NUMBER_SALE_LEVEL_REMAIN;

        int totalprice = (price10gold + price5gold + price3gold + lastPrice);

        return totalprice;
    }

    public void TurnGoldToCoin()
    {
        Console.WriteLine("Enter player name: ");
        playerName = Console.ReadLine();

        Console.Write("Enter your number of gold: ");
        goldAcess = Convert.ToInt32(Console.ReadLine());
        if (goldAcess < 0)
        {
            throw new ArgumentException("Please enter your gold higher than 0");
        }
        var NumberOfCoin = CheckGoldToCoin(goldAcess);
        Console.WriteLine("Total: " + NumberOfCoin);
    }
    public void GameMenu()
    {
        Console.WriteLine("------Welcome to Gold Digger game------");
        Console.WriteLine("| 1. Play.                            |");
        Console.WriteLine("| 2. View                             |");
        Console.WriteLine("| 3. Exit.                            |");
        Console.WriteLine("---------------------------------------");
        Console.WriteLine("\nEnter your choice: ");
    }
}