﻿
namespace Queue
{
    public class CallQueue
    {
        public int[] items;
        int top = -1;
        int bot = -1;
        int maxQueue;

        public CallQueue(int maxQueue)
        {
            this.maxQueue = maxQueue;
            items = new int[maxQueue];
        }


        public void EnQueue(int item)
        {
            if (top == -1 && bot == -1)
            {
                top = 0;
                bot = 0;
                items[bot] = item;
                return;
            }
            if (bot - top + 1 >= maxQueue)
            {
                throw new Exception("Queue is full");
            }

            bot++;
            items[bot] = item;
        }

        public int DeQueue()
        {
            if (top == -1 && bot == -1)
            {
                throw new Exception("Queue is empty");
            }
            int valueAtTop = items[top];
            if (top == bot)
            {
                top = -1;
                bot = -1;
            }
            else
            {
                top++;
            }
            return valueAtTop;
        }

        public int Peek()
        {
            if (bot == -1 && top == -1)
            {
                throw new Exception("Queue is empty");
            }
            return items[top];
        }

        public int Count() => bot - top + 1;
    }
}
