﻿using Exercise_BookStack;

BookManagement bookManagement = new BookManagement(5);

int choice = 0;
do
{
    Console.WriteLine("-----------Book Management------------");
    Console.WriteLine("| 1. Add Book.                       |");
    Console.WriteLine("| 2. Delete Book.                    |");
    Console.WriteLine("| 3. View Book.                      |");
    Console.WriteLine("| 4. Check quantity of Book.         |");
    Console.WriteLine("| 5. Exit.                           |");
    Console.WriteLine("------------------------------------- ");
    Console.WriteLine("\nEnter your choice: ");
    choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            bookManagement.AddBook("C# Basic", "John");
            bookManagement.AddBook("Java Fundamental", "Minh");
            bookManagement.AddBook("JavaScript", "Pham");
            bookManagement.AddBook("HTML And Css", "Huy");
            break;
        case 2:
            var removeBookAtTop = bookManagement.RemoveBookAtTop();
            Console.WriteLine(removeBookAtTop);
            break;
        case 3:
            var printBookAtTop = bookManagement.PrintBookAtTop();
            Console.WriteLine(printBookAtTop);
            break;
        case 4:
            var countElemmentInBook = bookManagement.CountElementInStack();
            Console.WriteLine($"Elemment of Book: {countElemmentInBook}");
            break;
        default:
            Console.WriteLine("Good bye!");
            break;
    }
} while (choice < 5);