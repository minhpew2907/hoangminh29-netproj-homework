﻿using Queue;

try
{
    //Khoi tao queue voi 5 ptu
    CallQueue queue = new CallQueue(5);
    //Them queue
    queue.EnQueue(1);
    queue.EnQueue(2);
    queue.EnQueue(3);
    queue.EnQueue(4);
    queue.EnQueue(5);
    //Lay item trong queue 
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    //item tiep theo lay ra queue
    Console.WriteLine("Peek: " + queue.Peek());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    Console.WriteLine("Dequeue: " + queue.DeQueue());
    //go het item ra

    //them 1 so  vao queue 
    queue.EnQueue(6);
    //boc item vua them ra
    Console.WriteLine("Dequeue: " + queue.DeQueue());
}catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}