USE MASTER
GO
DROP DATABASE IF EXISTS OnlineStore
GO
CREATE DATABASE OnlineStore
GO

USE OnlineStore
GO

CREATE TABLE Customers (
  CustomerID INT PRIMARY KEY IDENTITY(1, 1),
  FirstName VARCHAR(50) NOT NULL,
  LastName VARCHAR(50) NOT NULL,
  Email VARCHAR(100) UNIQUE NOT NULL,
  Address VARCHAR(200) NOT NULL
);

CREATE TABLE Orders (
  OrderID INT PRIMARY KEY IDENTITY(1, 1),
  OrderDate DATE NOT NULL,
  CustomerID INT FOREIGN KEY REFERENCES Customers(CustomerID),
  TotalAmount DECIMAL(10, 2) NOT NULL CHECK (TotalAmount > 100)

);

CREATE TABLE Product (
  ProductID INT PRIMARY KEY IDENTITY(1, 1),
  Name VARCHAR(100) NOT NULL,
  Description VARCHAR(200),
  Price DECIMAL(10, 2) NOT NULL CHECK (Price > 100)
);

CREATE TABLE Category (
  CategoryID INT PRIMARY KEY IDENTITY(1, 1),
  Name VARCHAR(50) NOT NULL,
  Description VARCHAR(200)
);

CREATE TABLE OrderItem (
  OrderID INT FOREIGN KEY REFERENCES Orders(OrderID),
  ProductID INT FOREIGN KEY REFERENCES Product(ProductID),
  Quantity INT NOT NULL DEFAULT 1,
  CONSTRAINT PK_OrderID_ProductID PRIMARY KEY (OrderID, ProductID),
);

CREATE TABLE ProductCategory (
  ProductID INT FOREIGN KEY REFERENCES Product(ProductID),
  CategoryID INT FOREIGN KEY REFERENCES Category(CategoryID),
  CONSTRAINT PK_ProductID_CategoryID PRIMARY KEY (ProductID, CategoryID)
);



INSERT INTO Customers (FirstName, LastName, Email, Address) 
VALUES ('Shellysheldon', 'Matschoss', 'smatschoss0@unicef.org', '697 Crescent Oaks Court'),
       ('Luisa', 'Pfleger', 'lpfleger1@51.la', '8621 Hayes Center'),
       ( 'Jean', 'Shawley', 'jshawley2@w3.org', '627 Canary Road'),
       ('Allyn', 'Hastie', 'ahastie3@wix.com', '3289 Prairie Rose Place'),
	   ('Fara', 'De Stoop', 'fdestoop4@fda.gov', '11425 Pepper Wood Point'),
	   ('Valma', 'Curnnokk', 'vcurnnokk5@google.com.au', '181 Lyons Trail'),
	   ('Shurwood', 'Mc Giffin', 'smcgiffin6@buzzfeed.com', '75 Brown Alley'),
	   ('Louis', 'Sherwyn', 'lsherwyn7@miibeian.gov.cn', '800 Heffernan Lane'),
       ('Grace', 'McCully', 'gmccully8@ucla.edu', '915 Vidon Way'),
       ('Ethyl', 'Irnis', 'eirnis9@toplist.cz', '913 Arapahoe Parkway');

INSERT INTO Orders (OrderDate,CustomerID, TotalAmount)
VALUES ('6/22/2023',1 , 159),
       ('6/27/2023',2 , 397),
	   ('2/17/2023',3 ,146),
       ('1/28/2023',4 ,488),
       ('6/29/2023',5 ,573),
	   ('5/7/2023',6 ,317),
	   ('6/15/2023',7 ,717),
	   ('12/18/2022',8 ,366),
	   ('5/17/2023',9 ,563),
	   ('12/4/2022',10 ,548);

INSERT INTO Product(Name, Description, Price)
VALUES ('Tommie Haydn', 'Pasta - Orecchiette', 776),
       ('Humfried Tremberth', 'Crackers - Melba Toast', 832),
	   ('Cosme Duchan', 'Sauce - Salsa', 567),
	   ('Olimpia Anfrey', 'Cherries - Maraschino,jar', 944),
	   ('Kate Jelleman', 'Chestnuts - Whole,canned', 443),
	   ('Devonne Wingate', 'Bread - Sour Batard', 472),
	   ('Brianna Ramsay', 'Pasta - Cannelloni, Sheets, Fresh', 194),
	   ('Veriee Renak', 'Bamboo Shoots - Sliced', 415),
	   ('Genny Cumine', 'English Muffin', 765),
	   ('Yul Wreakes', 'Butter Ripple - Phillips', 435);

INSERT INTO Category (Name, Description)
VALUES ('Fanny De Freyne', 'Health'),
	   ('Klaus Swendell', 'Tools'),
	   ('Wendie Olivi', 'Jewelry'),
	   ('Wenda Barlee', 'Shoes'),
	   ('Bettine Klugel', 'Baby'),
	   ('Barthel Broseman', 'Tools'),
	   ('Gunter McIleen', 'Tools'),
	   ('Annalee Cranson', 'Books'),
	   ('Maiga Goley', 'Clothing'),
	   ('Breanne Woodwing', 'Books');

INSERT INTO OrderItem  (OrderID, ProductID, Quantity) 
VALUES (1,1,12),
	   (2,2,54),
	   (3,3,8),
	   (4,4,54),
	   (5,5,72),
	   (6,6,59),
	   (7,7,44),
	   (8,8,47),
       (9,9,61),
	   (10,10,100);

INSERT INTO ProductCategory(ProductID, CategoryID)
VALUES (1,1),
	   (2,2),
	   (3,3),
	   (4,4),
	   (5,5),
	   (6,6),
	   (7,7),
	   (8,8),
	   (9,9),
	   (10,10);

--1 Th�m v�o t?ng b?ng 10 b?n ghi (s? d?ng l?nh insert multiple row), ch� � ??n IDENTITY c� th? v�o trang https://www.mockaroo.com/ ?? t?o ra d? li?u gi?

--SELECT:

--Vi?t truy v?n SQL ?? l?y t?t c? b?n ghi t? b?ng "Customers".
SELECT * 
FROM Customers

--Vi?t truy v?n SQL ?? l?y Customer ID, first name v� last name c?a kh�ch h�ng c� email l� 'example@example.com'.
SELECT CustomerID, FirstName, LastName, Email
FROM Customers
WHERE Email = 'smatschoss0@unicef.org';

--TOP:

--Vi?t truy v?n SQL ?? l?y 10 b?n ghi ??u ti�n t? b?ng "Product".
SELECT TOP 10 *
FROM Product

--Vi?t truy v?n SQL ?? l?y 5 b?n ghi ??u ti�n t? b?ng "Customers" v?i ?i?u ki?n ??a ch? l� '123 Main Street'.
SELECT TOP 5 *
FROM Customers
WHERE Address = '3289 Prairie Rose Place'

--PERCENT:

--Vi?t truy v?n SQL ?? l?y 25% b?n ghi ??u ti�n t? b?ng "Product" d?a tr�n gi� t?ng d?n.
SELECT TOP 25 PERCENT *
FROM Product 
ORDER BY Price ASC

--Vi?t truy v?n SQL ?? l?y 50% b?n ghi ??u ti�n t? b?ng "Orders" d?a tr�n ng�y ??t h�ng gi?m d?n.
SELECT TOP 50 PERCENT *
FROM Orders 
ORDER BY OrderDate DESC


--ORDER:

--Vi?t truy v?n SQL ?? l?y t?t c? b?n ghi t? b?ng "Product", s?p x?p theo gi� t?ng d?n.
SELECT *
FROM Product 
ORDER BY Price ASC

--Vi?t truy v?n SQL ?? l?y t?t c? b?n ghi t? b?ng "Orders", s?p x?p theo ng�y ??t h�ng gi?m d?n.
SELECT *
FROM Orders 
ORDER BY OrderDate DESC

--ALIAS:

--Vi?t truy v?n SQL ?? l?y first name v� last name c?a t?t c? kh�ch h�ng, v� hi?n th? k?t qu? v?i c�c ??nh danh c?t "First Name" v� "Last Name".
SELECT FirstName AS 'First Name', LastName AS 'Last Name'
FROM Customers

--Vi?t truy v?n SQL ?? l?y Product ID v� Price c?a t?t c? s?n ph?m, v� hi?n th? k?t qu? v?i c�c ??nh danh c?t "Product ID" v� "Price (USD)".
SELECT ProductID AS 'Product ID', Price AS 'Price'
FROM Product

--DISTINCT:

--Vi?t truy v?n SQL ?? l?y t?t c? gi� tr? duy nh?t c?a c?t "Address" t? b?ng "Customers".
SELECT DISTINCT Address
FROM Customers

--Vi?t truy v?n SQL ?? l?y t?t c? gi� tr? duy nh?t c?a c?t "Name" t? b?ng "Category".
SELECT DISTINCT Name
FROM Category
--WHERE:

--Vi?t truy v?n SQL ?? l?y t�n v� gi� c?a t?t c? s?n ph?m c� gi� l?n h?n 200.
SELECT Name, Price
FROM Product
WHERE Price > 200

--Vi?t truy v?n SQL ?? l?y first name v� last name c?a t?t c? kh�ch h�ng c� ??a ch? ch?
SELECT FirstName, LastName
FROM Customers
WHERE Address LIKE '%a%'