﻿Car existCar = new Car();

int choice = 0;
do
{
    existCar.Exist();
    existCar.CarMenu();
    choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            existCar.PrintCarInfo();
            break;
        case 2:
            existCar.AddNewCar();
            break;
        default:
            Console.WriteLine("See you again!");
            break;
    }
} while (choice < 3);

