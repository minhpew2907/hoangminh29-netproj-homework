﻿

namespace Animal
{
    public class Cat : Animals
    {
        public Cat(string name, int age, string type, string furcolor) : base(name, age, type)
        {
            _Name = name;
            _Age = age;
            _Type = type;
            _FurColor = furcolor;
        }

        public string _FurColor { get; set; }

        public void Meow()
        {
            Console.WriteLine("Meow! Meow!");
        }
    }
}
