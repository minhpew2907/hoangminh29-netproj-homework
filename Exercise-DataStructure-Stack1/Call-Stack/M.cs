﻿class M
{
    public int[] items;
    int maxStack;
    int top = -1;
    public M(int maxStack)
    {
        this.maxStack = maxStack;
        items = new int[maxStack];
    }

    public void Push(int item)
    {
        if (top == maxStack)
        {
            throw new Exception("Stack is full");
        }
        top++;   
        items[top] = item;

    }
    public int Pop()
    {
        if (top == -1)
        {
            throw new Exception("Stack is empty");
        }
        int topAtValue = items[top];
        top--;
        return topAtValue;
    }

    public int Top()
    {
        if (top == -1)
        {
            throw new Exception("Stack is empty");
        }

        return maxStack;
    }
    public bool isEmpty()
    {
        return top == -1;
    }
    public bool isFull()
    {
        return top == maxStack;
    }
    public int Count()
    {
        return top + 1;
    }
}

