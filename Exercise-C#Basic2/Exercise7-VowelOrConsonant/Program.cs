﻿using System.Reflection.PortableExecutable;

Console.WriteLine("Enter a word: ");
char word = char.Parse(Console.ReadLine().ToLower());
switch (word)
{
    case 'a':
        Console.WriteLine("is Vowel");
        break;
    case 'e':
        Console.WriteLine("is Vowel");
        break;
    case 'i':
        Console.WriteLine("is Vowel");
        break;
    case 'o':
        Console.WriteLine("is Vowel");
        break;
    case 'u':
        Console.WriteLine("is Vowel");
        break;
    default:
        Console.WriteLine("is Consonant");
        break;
}