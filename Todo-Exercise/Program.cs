﻿
using Microsoft.VisualBasic;
using Todo_Exercise;

TodoMangament todoMangament = new TodoMangament();

int choice = 0;
do
{
    await todoMangament.GetData();
    try
    {
        Console.WriteLine("------Todo management------");
        Console.WriteLine(" 1. View all task.         ");
        Console.WriteLine(" 2. View all date create.  ");
        Console.WriteLine(" 3. View all priority task.");
        Console.WriteLine(" 4. Add new task.          ");
        Console.WriteLine(" 5. Delete task.           ");
        Console.WriteLine(" 6. Task checking complete.");
        Console.WriteLine(" 7. Upgrade priority task. ");
        Console.WriteLine(" 8. Exit.                  ");
        Console.WriteLine("---------------------------");
        Console.WriteLine("Enter your choice:         ");
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                todoMangament.ViewTaskList();
                break;
            case 2:
                Console.WriteLine(todoMangament.ViewDateCreateTask());
                break;
            case 3:
                await todoMangament.ViewPriorityTask();
                break;
            case 4:
                Todo todo = new Todo();
                Console.WriteLine("Enter job id: ");
                todo.JobId = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter job name: ");
                todo.JobName = Console.ReadLine();
                await todoMangament.AddNewTask(todo);
                break;
            case 5:
                Console.WriteLine("Enter id you want to remove: ");
                var removeId = int.Parse(Console.ReadLine());
                await todoMangament.RemoveTask(removeId);
                break;
            case 6:
                Console.WriteLine("Enter id to checking: ");
                var updateId = int.Parse(Console.ReadLine());
                await todoMangament.TaskCheckingComplete(updateId);
                break;
            case 7:
                Console.WriteLine("Enter id to make it priority: ");
                var upgradeId = int.Parse(Console.ReadLine());
                await todoMangament.UpgradePriorityTask(upgradeId);
                break;
            case 8:
                Console.WriteLine("Gud bai!");
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Enter the number on list!");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message); ;
    }
} while (choice != 8);