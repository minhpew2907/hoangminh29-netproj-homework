﻿
enum PaperScissorRules
{
    SCISSOR = 1,
    ROCK = 2,
    PAPER = 3,
    TIE = 0
}

enum PlayerWhoWin
{
    PLAYER_1_WIN = 1,
    PLAYER_2_WIN = 2
}