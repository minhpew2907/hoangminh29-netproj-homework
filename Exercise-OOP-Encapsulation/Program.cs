﻿
using Exercise_Encapsulation;

Librabry librabry = new Librabry(100);

int choice = 0;
do
{
    try
    {
        librabry.Menu();
        Console.WriteLine("\nEnter your choice: ");
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Book book = new Book();
                Console.WriteLine("Enter book id: ");
                book._Id = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter book title: ");
                book._Title = Console.ReadLine();
                Console.WriteLine("Enter book author: ");
                book._Author = Console.ReadLine();
                book._Available = true;
                librabry.AddBook(book);
                break;
            case 2:
                Console.WriteLine(librabry.PrintBookInfo());
                break;
            case 3:
                Console.WriteLine("Enter id to rented a book: ");
                int id = int.Parse(Console.ReadLine());
                librabry.CheckOut(id);
                break;
            case 4:
                Console.WriteLine("Enter id to take back a book: ");
                int id2 = int.Parse(Console.ReadLine());
                librabry.ReturnBook(id2);
                break;
            default:
                Console.WriteLine("Good bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);