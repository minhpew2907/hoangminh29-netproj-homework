﻿const int NUMBER_SALE_LEVEL1 = 10;
const int NUMBER_SALE_LEVEL1_PRICE = 10;
const int NUMBER_SALE_LEVEL2 = 5;
const int NUMBER_SALE_LEVEL2_PRICE = 5;
const int NUMBER_SALE_LEVEL3 = 3;
const int NUMBER_SALE_LEVEL3_PRICE = 2;
const int NUMBER_SALE_LEVEL_REMAIN = 1;


Console.WriteLine("Nhap so quang vang ban thu duoc: ");
int goldAcess = int.Parse(Console.ReadLine());
int firstGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL1);
int price10gold = firstGold * NUMBER_SALE_LEVEL1_PRICE;
goldAcess -= firstGold;

int secondGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL2);
int price5gold = secondGold * NUMBER_SALE_LEVEL2_PRICE;
goldAcess -= secondGold;

int thirdGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL3);
int price3gold = thirdGold * NUMBER_SALE_LEVEL3_PRICE;
goldAcess -= thirdGold;

//int lastOre = Math.Min(goldAcess, 11);
int lastPrice = goldAcess * NUMBER_SALE_LEVEL_REMAIN;
//goldAcess -= lastOre;

int totalprice = (price10gold + price5gold + price3gold + lastPrice);
Console.WriteLine("Total money you received: " + totalprice);

