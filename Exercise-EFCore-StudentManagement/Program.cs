﻿
using Exercise_EFCore_EmployeeManagement;
using System.Transactions;

StudentManagement studentManagement = new StudentManagement();



int choice = 0;
do
{
    try
    {
        Console.WriteLine("-----Student management-----");
        Console.WriteLine(" 1. Add Student. ");
        Console.WriteLine(" 2. View list student. ");
        Console.WriteLine(" 3. Edit student. ");
        Console.WriteLine(" 4. Delete student. ");
        Console.WriteLine(" 5. Search student. ");
        Console.WriteLine(" 6. Statistic student. ");
        Console.WriteLine(" 7. Exit. ");
        Console.WriteLine("----------------------------");
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Student student = new Student();
                Console.WriteLine("Enter student name: ");
                student.Name = Console.ReadLine();
                Console.WriteLine("Enter student age: ");
                student.Age = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter student major: ");
                student.Major = Console.ReadLine();
                Console.WriteLine("Enter student gender: ");
                student.Gender = Console.ReadLine();
                studentManagement.AddStudent(student);
                break;
            case 2:
                var view = studentManagement.ViewStudentList();
                Console.WriteLine(view);
                break;
            case 3:
                Console.WriteLine("Enter your id: ");
                int id = int.Parse(Console.ReadLine());
                studentManagement.EditStudent(id);
                break;
            case 4:
                Console.WriteLine("Enter your id: ");
                int idDelete = int.Parse(Console.ReadLine());
                studentManagement.DeleteStudent(idDelete);
                break;
            case 5:
                Console.WriteLine("Enter student name: ");
                string nameSearch = Console.ReadLine();
                var search = studentManagement.SearchStudent(nameSearch);
                Console.WriteLine(search);
                break;
            case 6:
                studentManagement.StatisticStudent();
                break;
            case 7:
                Console.WriteLine("Good bye!");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Please enter in the range of menu");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 7);