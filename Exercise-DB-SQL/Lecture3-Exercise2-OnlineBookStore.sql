﻿USE MASTER
GO
DROP DATABASE IF EXISTS OnlineBookstore
GO
-- Tạo cơ sở dữ liệu
CREATE DATABASE OnlineBookstore
GO

-- Sử dụng cơ sở dữ liệu
USE OnlineBookstore;

-- Tạo bảng Books
CREATE TABLE Books (
	ID INT PRIMARY KEY IDENTITY(1, 1),
	Title VARCHAR(255) NOT NULL,
	Author VARCHAR(255) NOT NULL,
	Price DECIMAL(10,2) NOT NULL CHECK (Price > 0),
	PublishedDate DATE NOT NULL
);

-- Tạo bảng Customers
CREATE TABLE Customers (
	ID INT PRIMARY KEY IDENTITY(1, 1),
	FirstName VARCHAR(255) NOT NULL,
	LastName VARCHAR(255) NOT NULL,
	Email VARCHAR(255) UNIQUE NOT NULL,
	Phone VARCHAR(20) NOT NULL,
	DOB DATE NOT NULL
);

-- Tạo bảng Orders
CREATE TABLE Orders (
	ID INT PRIMARY KEY IDENTITY(1, 1),
	CustomerID INT FOREIGN KEY REFERENCES Customers(ID),
	OrderDate DATE NOT NULL,
	TotalAmount DECIMAL(10,2) NOT NULL CHECK (TotalAmount > 0),
	-- FOREIGN KEY (CustomerID) REFERENCES Customers(ID) các khác để thêm khóa ngoại
);

-- Tạo bảng OrderDetail
CREATE TABLE OrderDetail (
	OrderID INT FOREIGN KEY REFERENCES Orders(ID),
	BookID INT FOREIGN KEY REFERENCES Books(ID),
	NumberOfBook INT NOT NULL DEFAULT 1,
	CONSTRAINT PK_OrderID_BookID PRIMARY KEY(OrderID, BookID)
);


--1 Thêm vào từng bảng 10 bản ghi (sử dụng lệnh insert multiple row), chú ý đến IDENTITY có thể vào trang https://www.mockaroo.com/ để tạo ra dữ liệu giả
INSERT INTO Books (Title, Author, Price, PublishedDate)
VALUES ('Paths of Glory', 'Tine Shefton', 55, '11/3/2022'),
       ('Jubilation Street', 'Siana Rolf', 29, '9/13/2022'),
       ('Château, The', 'Morgen Guinn', 13, '12/8/2022'),
       ('Yes Men, The', 'Aggie Beldan', 64, '7/21/2023'),
       ('Playing ''In the Company of Men'' (En jouant ''Dans la compagnie des hommes'')', 'Bjorn Cocci', 32, '2/10/2023'),
       ('Three of Hearts', 'Siana Rolf', 91, '7/23/2023'),
       ('Marilyn Hotchkiss'' Ballroom Dancing & Charm School', 'Oren Frangione', 11, '4/27/2023'),
       ('Yes', 'Danya Hulks', 87, '12/17/2022'),
       ('Yes, Madam (a.k.a. Police Assassins) (a.k.a. In the Line of Duty 2) (Huang gu shi jie)', 'Kristoffer Darcey', 44, '8/31/2022'),
       ('French Fried Vacation (Les Bronzés)', 'Langsdon Fessions', 59, '7/30/2023');

INSERT INTO Customers (FirstName, LastName, Email, Phone, DOB) 
VALUES ('Renado', 'Emmanuele', 'remmanuele0@goo.ne.jp', '651-902-6207', '2/1/2023'),
	   ('Harv', 'Pantling', 'hpantling1@redcross.org', '702-629-8693', '5/3/2023'),
	   ('Benoite', 'Tancock', 'btancock2@bloglines.com', '560-326-5005', '4/25/2023'),
	   ('Heidi', 'Jiggens', 'hjiggens3@networkadvertising.org', '830-742-6047', '2/24/2023'),
	   ('Birk', 'Harsnep', 'bharsnep4@globo.com', '970-773-5448', '1/18/2023'),
	   ('Cassie', 'Coomer', 'ccoomer5@google.es', '831-732-4832', '10/6/2022'),
	   ('Nickolas', 'Zouch', 'nzouch6@sina.com.cn', '563-429-3875', '4/3/2023'),
	   ('Garrett', 'Kelsey', 'gkelsey7@blinklist.com', '161-665-9531', '6/14/2023'),
	   ('Brittaney', 'McAtamney', 'bmcatamney8@oakley.com', '661-137-6819', '6/21/2023'),
	   ('Robinette', 'Hansberry', 'rhansberry9@com.com', '556-303-4617', '7/28/2023');

INSERT INTO Orders (CustomerID, OrderDate, TotalAmount)
VALUES (1,'5/17/2023', 69),
       (2,'2/8/2023', 46),
	   (3,'7/17/2023', 99),
	   (4,'6/2/2023', 52),
	   (5,'7/11/2023', 95),
	   (6,'2/27/2023', 7),
	   (7,'8/14/2022', 69),
	   (8,'7/17/2023', 28),
	   (9,'8/23/2022', 46),
	   (10,'3/30/2023', 16);
INSERT INTO OrderDetail(OrderID, BookID, NumberOfBook)
VALUES (1,1,89),
       (2,2,100),
	   (3,3,80),
	   (4,4,11),
	   (5,5,72),
	   (6,6,96),
	   (7,7,26),
	   (8,8,28),
	   (9,9,44),
	   (10,10,40);
--SELECT
--a. Lấy tất cả sách từ bảng Books.
SELECT * FROM Books
--b. Lấy tất cả khách hàng từ bảng Customers.
SELECT * FROM Customers
--c. Lấy tất cả đơn hàng từ bảng Orders.
SELECT * FROM Orders
--d. Lấy tất cả chi tiết đơn hàng từ bảng OrderDetail.
SELECT * FROM OrderDetail
--e. Lấy sách có ID 10 từ bảng Books. 
SELECT *
FROM Books
WHERE ID = 10
--f. Lấy khách hàng có ID 10 từ bảng Customers.
SELECT *
FROM Customers
WHERE ID = 10
--g. Lấy đơn hàng có ID 10 từ bảng Orders.
SELECT *
FROM Orders
WHERE ID = 10
--h. Lấy chi tiết đơn hàng có OrderID 1 từ bảng OrderDetail.
SELECT *
FROM OrderDetail
WHERE OrderID = 1

--3. UPDATE
--a. Cập nhật giá sách có ID 5 thành 14.99.
SELECT * FROM Books
UPDATE Books
SET Price = 14.99
WHERE ID = 5
--b. Cập nhật email của khách hàng có ID 2 thành "newemail@gmail.com".
SELECT * FROM Customers
UPDATE Customers
SET Email = 'MinhPew2907@gmail.com'
WHERE ID = 2
--c. Cập nhật tổng giá trị của đơn hàng có ID 4 thành 30.99.
SELECT * FROM Orders
UPDATE Orders
SET TotalAmount = 30.99
WHERE ID = 4
--4. DELETE
--a. Xóa sách có ID 10 từ bảng Books.
DELETE FROM OrderDetail
WHERE OrderID = 10
DELETE FROM Books
WHERE ID = 10
SELECT * FROM Books
--b. Xóa khách hàng có ID 10 từ bảng Customers.
DELETE FROM Orders
WHERE CustomerID = 10
DELETE FROM Customers
WHERE ID = 10
SELECT * FROM Customers
--c. Xóa đơn hàng có ID 10 từ bảng Orders (bao gồm cả chi tiết đơn hàng).
DELETE FROM Orders
WHERE ID = 10 
DELETE FROM OrderDetail
WHERE OrderID = 10
SELECT * FROM Orders
SELECT * FROM OrderDetail
--ADDITIONAL
--Viết câu lệnh SELECT để lấy 10 cuốn sách có giá cao nhất từ bảng Books.
SELECT TOP 10 *
FROM Books
ORDER BY Price DESC 
--Viết câu lệnh SELECT để lấy "Title và Price" của tất cả sách, sử dụng tên định danh "BookName" cho cột Title.
SELECT Title AS 'BookName', Price 
FROM Books
--Viết câu lệnh SELECT để lấy tất cả tên tác giả không trùng lặp từ bảng Books.
SELECT DISTINCT Author
FROM Books
--Viết VIEW hiển thị BookID, Book Title, Author và Published Date cho tất cả sách trong bảng Books.
CREATE VIEW MINHPEW_VIEW AS
SELECT ID, Title, Author, PublishedDate
FROM Books
SELECT * FROM MINHPEW_VIEW
--Viết câu lệnh SELECT INTO để tạo một bảng mới có tên "ExpensiveBooks" chứa các cột BookID, Title và Price của tất cả sách có giá lớn hơn $50.00
SELECT ID, Title, Price
INTO ExpensiveBooks
FROM Books
WHERE Price > $50.00
SELECT * FROM ExpensiveBooks
--Operators cơ bản:
--Viết truy vấn lấy tất cả khách hàng có tên chứa chuỗi "John".(gợi ý LIKE)
SELECT * 
FROM Customers
WHERE FirstName LIKE '%Re%' OR LastName LIKE '%Hp%'
--Viết truy vấn lấy tất cả sách có giá lớn hơn hoặc bằng $20.
SELECT * 
FROM Books
WHERE Price >= $20
--Viết truy vấn lấy tất cả khách hàng có số điện thoại khác "555-1234".
SELECT *
FROM Customers
WHERE Phone != '560-326-5005'
--Viết truy vấn lấy tất cả đơn hàng có tổng giá trị nhỏ hơn $100.
SELECT *
FROM Orders
WHERE TotalAmount < $90
--Operators logic:
--Viết truy vấn lấy tất cả khách hàng có tên chứa chuỗi "John" và địa chỉ email chứa chuỗi "gmail.com".
SELECT * 
FROM Customers
WHERE FirstName = 'Harv' OR LastName = 'Harsnep' AND Email = 'gmail.com'