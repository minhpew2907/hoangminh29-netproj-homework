﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo_Exercise
{
    public class Todo
    {
        private int priority = 1;
        private bool isComplete = false;
        private DateTime dateCreate = DateTime.Now;
        private DateTime? dateComplete;

        public int JobId { get; set; }
        public string JobName { get; set; }
        public DateTime DateCreate
        {
            get => dateCreate;
            set
            {
                dateCreate = value;
            }
        }
        public DateTime? DateComplete
        {
            get => dateComplete;
            set
            {
                dateComplete = value; 
            }
        }
        public bool IsCompleted
        {
            get => isComplete;
            set
            {
                isComplete = value;
            }
        }
        public int Priority
        {
            get => priority;
            set
            {
                if (value < 1 || value > 8)
                {
                    throw new Exception("Out of range[1-8]");
                }
                priority = value;
            }
        }
    }
}
