﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_C_Acvanced_Square_Cube
{
    public class SignatureManager : DelegateCustom
    {
        protected int PrintResult(Custom custom, int number)
        {
            return custom(number);
        }

        private int Input()
        {
            Console.WriteLine("Enter a number: ");
            int number = int.Parse(Console.ReadLine());
            return number;
        }

        public void DoubleNumber()
        {
            int number = Input();
            var result = PrintResult(Double, number);
            Console.WriteLine(result);
        }

        public void TripleNumber()
        {
            int number = Input();
            var result = PrintResult(Triple, number);
            Console.WriteLine(result);
        }

        public void SquareNumber()
        {
            int number = Input();
            var result = PrintResult(Square, number);
            Console.WriteLine(result);

        }

        public void CubeNumber()
        {
            int number = Input();
            var result = PrintResult(Cube, number);
            Console.WriteLine(result);
        }

        int Double(int number) => number * 2;
        int Triple(int number) => number * 3;
        int Square(int number) => number * number;
        int Cube(int number) => number * number * number;
    }
}
