﻿class GameFile
{
    const string FILE_PATH = @"C:\Users\ADMIN\Desktop\NET j Pro ram\Exercise-C#BasicFullCourse\Exercise-C#Basic7\Exercise4-TroChoiDoanSoAvanced\minh.txt";

    public bool ExistFile()
    {
        if (File.Exists(FILE_PATH))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ReadFile()
    {
        if (ExistFile())
        {
            string fileContent = File.ReadAllText(FILE_PATH);
            Console.WriteLine(fileContent);
        }
        else
        {
            throw new Exception("File not exist");
        }
    }
    public void WriteFile()
    {
        string playerName = RandomGame.playerName;
        int rankedGame = RandomGame.rankedGame;
        if (ExistFile())
        {
            string content = "Player Name: " + playerName + "\nRank: " + rankedGame;
            File.WriteAllText(FILE_PATH, content);
        }
        else
        {
            throw new Exception("File not exist");
        }
    }
}