use master 
go 
drop database if exists ProductManagement
go
create database ProductManagement
go
use ProjectManagement
go

create table Products
(
	ID int primary key identity(1, 1),
	ProductName varchar(255),
	Price int,
	Description varchar(255),
	TradeMark varchar(20)
);

create table Orders
(
	ID int primary key identity(1, 1),
	OrderID int,
	OrderDate date,
	OrderAddress varchar(255),
	foreign key (OrderID) references Products(ID)
);

create table Customer
(
	ID int primary key identity(1, 1),
	CustomerID int,
	Name varchar(255),
	Address varchar(255),
	PhoneNumber varchar(20),
	foreign key (CustomerID) references Orders(ID),
);

create table Addressed
(
    ID int primary key identity(1, 1),
	Address varchar(255),
	City varchar(255),
	Disrict varchar(255),
	Street varchar(255)
);


