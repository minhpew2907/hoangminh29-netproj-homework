﻿

using OOP_Abstract_Shape;

Shape[] shape = new Shape[9];
shape[0] = new Rectangle(2, 3);
shape[1] = new Rectangle(5, 6);
shape[2] = new Rectangle(7, 8);
shape[3] = new Triangle(2, 3, 4);
shape[4] = new Triangle(5, 6, 7);
shape[5] = new Triangle(8, 9, 5);
shape[6] = new Circle(29);
shape[7] = new Circle(7);
shape[8] = new Circle(6);

foreach (Shape s in shape)
{
    Console.WriteLine($"\n{s.GetType().Name}");
    Console.WriteLine("Area: " + s.CalculateArea());
    Console.WriteLine("Perimeter: " + s.CalculatePerimeter());
}