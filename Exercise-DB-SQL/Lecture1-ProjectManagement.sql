use master 
go
drop database if exists ProjectManagement
go
create database ProjectManagement
go
use ProjectManagement
go

create table Employees
(	
	EmployeeID int primary key identity(1, 1),
	PhoneNumber varchar(20),
	Email varchar(225)
);

create table Projects
(
	ProjectID int references Employees(EmployeeID),
	DatePublished date,
	DateEnd date,
	Salary decimal(10, 2),
	ProjectName varchar(255)
);

drop table Employees;