﻿Console.WriteLine("Enter a number: ");
int number = int.Parse(Console.ReadLine());
if (number >= 1 && number <= 100)
{
    Console.WriteLine("The number is in range 1...100");
}
else
{
    Console.WriteLine("The number is not in range 1...100");
}