﻿RandomGame randomGame = new RandomGame();
GameFile file = new GameFile();

int choice = 0;

do
{
    try
    {
        randomGame.Menu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                randomGame.Input();
                file.WriteFile();
                break;
            case 2:
                file.ReadFile();
                break;
            case 3:
                Console.WriteLine("See You again!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice < 3);


