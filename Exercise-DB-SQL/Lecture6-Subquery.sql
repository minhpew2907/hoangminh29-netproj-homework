﻿USE MASTER
GO
DROP DATABASE IF EXISTS LECTURE6_SubQueries
GO
CREATE DATABASE LECTURE6_SubQueries
GO
USE LECTURE6_SubQueries
GO

-- Tạo bảng "Customers"
CREATE TABLE Customers (
    ID INT PRIMARY KEY IDENTITY(1, 1),
    Name NVARCHAR(50) NOT NULL,
    Age INT,
    Email NVARCHAR(100)
);

-- Tạo bảng "Products"
CREATE TABLE Products (
    ID INT PRIMARY KEY IDENTITY(1, 1),
    Name NVARCHAR(50) NOT NULL,
    Price DECIMAL(10, 2) NOT NULL,
    StockQuantity INT NOT NULL
);


-- Tạo bảng "Orders"
CREATE TABLE Orders (
    ID INT PRIMARY KEY IDENTITY(1, 1),
    CustomerID INT,
    OrderDate DATETIME
);


-- Tạo bảng "OrderDetails"
CREATE TABLE OrderDetails (
    ID INT PRIMARY KEY IDENTITY(1, 1),
    OrderID INT,
    ProductID INT,
    Quantity INT
);


-- Chèn dữ liệu vào bảng "Customers"
INSERT INTO Customers (Name, Age, Email)
VALUES
    ('John Smith', 35, 'john.smith@example.com'),
    ('Jane Doe', 28, 'jane.doe@example.com'),
    ('Michael Johnson', 40, 'michael.johnson@example.com'),
    ('Emily Wilson', 22, 'emily.wilson@example.com'),
    ('David Lee', 32, 'david.lee@example.com'),
    ('Sophia Kim', 30, 'sophia.kim@example.com'),
    ('William Brown', 45, 'william.brown@example.com'),
    ('Olivia Martinez', 29, 'olivia.martinez@example.com'),
    ('James Anderson', 38, 'james.anderson@example.com'),
    ('Emma Taylor', 27, 'emma.taylor@example.com');

-- Chèn dữ liệu vào bảng "Products"
INSERT INTO Products (Name, Price, StockQuantity)
VALUES
    ('Laptop', 1200.00, 50),
    ('Smartphone', 800.00, 100),
    ('Tablet', 500.00, 75),
    ('Headphones', 100.00, 200),
    ('Keyboard', 50.00, 150),
    ('Mouse', 30.00, 100),
    ('Monitor', 300.00, 60),
    ('Printer', 150.00, 30),
    ('Camera', 600.00, 40),
    ('Speaker', 80.00, 120);

-- Chèn dữ liệu vào bảng "Orders"
INSERT INTO Orders (CustomerID, OrderDate)
VALUES
    (1, '2023-08-01 10:00:00'),
    (2, '2023-09-02 15:30:00'),
    (3, '2023-06-03 09:45:00'),
    (4, '2023-05-04 12:20:00'),
    (5, '2023-04-05 11:10:00'),
    (6, '2023-07-06 14:00:00'),
    (7, '2023-07-07 17:20:00'),
    (8, '2023-07-08 08:30:00'),
    (9, '2023-07-09 13:15:00'),
    (10, '2023-07-10 16:40:00');

-- Chèn dữ liệu vào bảng "OrderDetails"
INSERT INTO OrderDetails (OrderID, ProductID, Quantity)
VALUES
    (1, 1, 2),
    (1, 3, 1),
    (1, 2, 1),
    (1, 5, 3),
    (3, 4, 1),
    (3, 7, 2),
    (3, 3, 1),
    (3, 6, 1),
    (5, 1, 3),
    (5, 8, 1),
    (6, 5, 2),
    (6, 9, 1),
    (7, 2, 1),
    (7, 3, 2),
    (8, 1, 2),
    (8, 4, 2),
    (9, 2, 1),
    (9, 6, 3),
    (9, 7, 1),
    (9, 8, 1);

SELECT 
    c.ID AS CustomerID,
    c.Name AS CustomerName,
    c.Age,
    c.Email,
    p.ID AS ProductID,
    p.Name AS ProductName,
    p.Price,
    p.StockQuantity,
    o.ID AS OrderID,
    o.OrderDate,
    od.Quantity
FROM 
    Customers c
INNER JOIN 
    Orders o ON c.ID = o.CustomerID
INNER JOIN 
    OrderDetails od ON o.ID = od.OrderID
INNER JOIN 
    Products p ON od.ProductID = p.ID;

--Viết các câu truy vấn sử dụng subqueries và các toán tử advanced như EXISTS, IN, ANY, ALL để thực hiện các yêu cầu sau mỗi câu có thể làm bằng nhiều cách:

--Chọn tất cả khách hàng có đơn hàng trong bảng Orders.
SELECT DISTINCT *
FROM Customers c
WHERE EXISTS (SELECT * 
			  FROM Orders o
	       	  WHERE o.CustomerID = c.ID)
--Chọn tất cả khách hàng có tuổi lớn hơn tuổi trung bình của tất cả khách hàng.
SELECT DISTINCT *
FROM Customers c
WHERE c.Age > (SELECT AVG(c2.Age)
			   FROM Customers c2)
--Chọn tên của tất cả sản phẩm có giá cao hơn giá trung bình của tất cả sản phẩm.
SELECT DISTINCT p.Name, p.Price
FROM Products p
WHERE p.Price > (SELECT AVG(p2.Price)
				 FROM Products p2)
--Chọn tên và giá của tất cả sản phẩm đã được đặt hàng bởi khách hàng có địa chỉ email chứa "@example.com".
SELECT DISTINCT p.Name, p.Price
FROM Products p
WHERE EXISTS (SELECT c.Email
			  FROM Customers c
			  WHERE c.Email LIKE '%@example.com%')
--Chọn tên của tất cả khách hàng đã đặt hàng ít nhất một lần trong tháng 7 năm 2023.
SELECT DISTINCT c.Name, o.OrderDate
FROM Customers c
INNER JOIN Orders o ON c.ID = o.CustomerID
WHERE EXISTS (SELECT *
			  FROM OrderDetails od
			  WHERE od.OrderID = o.ID AND MONTH(o.OrderDate) IN (7)  AND YEAR(o.OrderDate) IN (2023))
--Chọn tên của tất cả khách hàng mà đã đặt hàng tất cả các sản phẩm đều có giá lớn hơn 50.
SELECT c.Name
FROM Customers c
WHERE EXISTS (SELECT *
			  FROM Products p
			  WHERE p.Price > 50) 
--Chọn tên của tất cả sản phẩm đã được đặt hàng nhiều hơn số lượng trung bình của tất cả sản phẩm.
SELECT DISTINCT p.Name, od.Quantity
FROM Products p
INNER JOIN OrderDetails od on p.ID = od.ProductID
WHERE od.Quantity > ANY (SELECT AVG(od.Quantity) FROM OrderDetails od)
GROUP BY  p.Name, od.Quantity
 
--Chọn tên của tất cả sản phẩm đã được đặt hàng bởi ít nhất một khách hàng có tuổi lớn hơn 30.
SELECT p.Name, c.Age
FROM Products p
INNER JOIN Customers c ON p.ID = c.ID
WHERE c.Age > ANY (SELECT c.Age
				   FROM Customers c
				   WHERE c.Age = 30)
GROUP BY p.Name, c.Age