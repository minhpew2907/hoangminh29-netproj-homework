﻿using Bank_UseQueue;

BankManagement bankManagement = new BankManagement(9);

int choice = 0;
do
{
    try
    {
        Console.WriteLine("----------Bank Management---------");
        Console.WriteLine("| 1. Add Customer on wait list.  |");
        Console.WriteLine("| 2. Call the next customer.     |");
        Console.WriteLine("| 3. View the customer.          |");
        Console.WriteLine("| 4. Statistic table.            |");
        Console.WriteLine("| 5. Exit.                       |");
        Console.WriteLine("----------------------------------");
        Console.WriteLine("\nEnter your choice: ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                Bank bank = new Bank();
                bankManagement.CustomerFillIn(bank);
                bankManagement.AddCustomer(bank);
                break;
            case 2:
                var nextCustomer = bankManagement.CallNextCustomer();
                Console.WriteLine("Customer: " + nextCustomer.CustomerName + " |" + " Money Withdraw: " + nextCustomer.MoneyWithdraw);
                break;
            case 3:
                var viewCustomer = bankManagement.ViewThreeCustomer();
                Console.WriteLine(viewCustomer);
                break;
            case 4:
                break;
            default:
                Console.WriteLine("Good Bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }

} while (choice < 5);
