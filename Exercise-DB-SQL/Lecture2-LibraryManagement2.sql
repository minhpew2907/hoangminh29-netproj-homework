use master
go
drop database if exists LibraryManagement2
go 
create database LibraryManagement2
go
use LibraryManagement2
go

create table Books
(
	ID int primary key identity (1, 1),
	Title varchar(255),
	Author varchar(255),
	Price decimal(10, 2),
	PublishedDate date 
);

create table Customers
(
	ID int primary key identity (1,1),
	Name varchar(255),
	Email varchar(255),
	Phone varchar(20),
);

create table Orders
(
	ID int primary key identity (1, 1),
	CustomerID int foreign key references Customers(ID),
	OrderDate date,
	TotalAmount decimal(10, 2)
);

create table OrderDetail
(
	ID int primary key identity (1, 1),
	OrderID int foreign key references Orders(ID),
	BookID int foreign key references Books(ID),
	Count int 
);

drop table Books;