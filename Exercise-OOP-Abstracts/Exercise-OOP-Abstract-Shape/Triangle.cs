﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Abstract_Shape
{
    public class Triangle : Shape
    {
        private double _a;
        private double _b;
        private double _c;

        public Triangle(double a, double b, double c)
        {
            _a = a;
            _b = b;
            _c = c;
        }

        public override double CalculateArea()
        {
            double p = (_a + _b + _c) / 2;
            double area = Math.Sqrt(p * (p - _a) * (p - _b) * (p - _c));
            return area;
        }

        public override double CalculatePerimeter()
        {
            return _a + _b + _c;
        }
    }
}
