﻿
Circle circle = new Circle(3.5f);
//circle.GetArea();
circle.View();
Circle circle2 = new Circle(5.2f);
//circle2.GetCircumference();
circle2.View2();

class Circle
{
    public float Radius;

    public Circle()
    {

    }

    public Circle(float radius)
    {
        Radius = radius;
    }
    public float GetArea()
    {
        return (float)((float)Math.PI * Math.Pow(Radius,2));
    }
    public float GetCircumference()
    {
        return 2 * (float)Math.PI * Radius;
    }
    public void View()
    {
        Console.WriteLine("Area: " + GetArea());
    }
    public void View2()
    {
        Console.WriteLine("Circumference: " + GetCircumference());
    }
}