﻿
M stackdemo = new M(7);
stackdemo.Push(1);
stackdemo.Push(2);
stackdemo.Push(3);
stackdemo.Push(4);
stackdemo.Push(5);
stackdemo.Push(6);
stackdemo.Push(7);

Console.WriteLine(stackdemo.Pop());
Console.WriteLine(stackdemo.Pop());
Console.WriteLine(stackdemo.Pop());
Console.WriteLine(stackdemo.Pop());

Console.WriteLine("Top index vaue: " + stackdemo.Top());
Console.WriteLine("Count: " + stackdemo.Count());
