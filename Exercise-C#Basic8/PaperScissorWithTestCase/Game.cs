﻿using System.Data;

namespace PaperScissorTest
    
{
    public class Game
    {
        public static int ranked = 0;
        public static string player1Name;
        public static string player2Name;
        public static int player1Score;
        public static int player2Score;

        public int PaperScissorRule(int player1, int player2)
        {
            
            if (player1 == (int)PaperScissorRules.SCISSOR && player2 == (int)PaperScissorRules.PAPER
                || player1 == (int)PaperScissorRules.ROCK && player2 == (int)PaperScissorRules.SCISSOR
                || player1 == (int)PaperScissorRules.PAPER && player2 == (int)PaperScissorRules.ROCK)
            {
                return (int)PlayerWhoWin.PLAYER_1_WIN;
            }
            if (player2 == (int)PaperScissorRules.SCISSOR && player1 == (int)PaperScissorRules.PAPER
                || player2 == (int)PaperScissorRules.ROCK && player1 == (int)PaperScissorRules.SCISSOR
                || player2 == (int)PaperScissorRules.PAPER && player1 == (int)PaperScissorRules.ROCK)
            {
                return (int)PlayerWhoWin.PLAYER_2_WIN;
            }
            return (int)PaperScissorRules.TIE;
        }

        public int CheckResultWhoWin(int result)
        {
            if (result == (int)PaperScissorRules.TIE)
            {
                Console.WriteLine("TIE");
            }
            else if (result == (int)PlayerWhoWin.PLAYER_1_WIN)
            {
                Console.WriteLine("Player 1 win");
                player1Score++;
            }
            else
            {
                Console.WriteLine("Player 2 win");
                player2Score++;
            }
            return result;
        }
        public int CheckRankedWhoWin()
        {
            if (player1Score > player2Score)
            {
                ranked = player1Score;
            }
            else if (player1Score < player2Score)
            {
                ranked = player2Score;
            }
            return ranked;
        }

        public void ViewChoosen()
        {
            Console.WriteLine("1. SCISSOR");
            Console.WriteLine("2. ROCK");
            Console.WriteLine("3. PAPER");
        }

        public void Playing()
        {
            Console.WriteLine("Player 1 Name: ");
            player1Name = Console.ReadLine();
            Console.WriteLine("Player 2 Name: ");
            player2Name = Console.ReadLine();

            ViewChoosen();
            int player1Choice = 0;
            int player2Choice = 0;

            Console.WriteLine("Player 1 Choose: ");
            player1Choice = Convert.ToInt32(Console.ReadLine());
            if (player1Choice > 3 || player1Choice < 1)
            {
                throw new ArgumentException("Please choosing in range!");
            }
            Console.WriteLine("Player 2 Choose: ");
            player2Choice = Convert.ToInt32(Console.ReadLine());
            if (player2Choice > 3 || player2Choice < 1)
            {
                throw new ArgumentException("Please choosing in range!");
            }
            int checkResult = PaperScissorRule(player1Choice, player2Choice);
            Console.WriteLine(CheckResultWhoWin(checkResult));
        }
        public void GameMenu()
        {
            Console.WriteLine("--------Paper Scissor Game-------");
            Console.WriteLine("| 1. Play.                      |");
            Console.WriteLine("| 2. View player name & ranked  |");
            Console.WriteLine("| 3. Exit.                      |");
            Console.WriteLine("---------------------------------");
        }
    }
}
