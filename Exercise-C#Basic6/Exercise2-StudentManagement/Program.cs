﻿
Student stu = new Student("John", 15, 9);
stu.GetInfo();
stu.SetGrade(10);
stu.GetInfo();

class Student
{
    public Student()
    {

    }

    public Student(string name, int age, int grade)
    {
        Name = name;
        Age = age;
        Grade = grade;
    }

    public int SetGrade(int grade)
    {
       return Grade = grade;
    }
    public void GetInfo()
    {
        Console.WriteLine("Student name: " + Name);
        Console.WriteLine("Student Age: " + Age);
        Console.WriteLine("Student Grade: " + Grade);
    }

    public string Name;
    public int Age;
    public int Grade;
}


