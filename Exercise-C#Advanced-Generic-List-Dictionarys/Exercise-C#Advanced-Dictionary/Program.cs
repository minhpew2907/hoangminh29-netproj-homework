﻿using Exercise_C_Acvanced_Dictionary;

DictionaryManager dictionaryManager = new DictionaryManager();

int choice = 0;
do
{
    try
    {
        Console.WriteLine("------Dictionary------");
        Console.WriteLine(" 1. Add a new word.   ");
        Console.WriteLine(" 2. Search a word.    ");
        Console.WriteLine(" 3. Remove a word.    ");
        Console.WriteLine(" 4. View list of word.");
        Console.WriteLine(" 5. Exit.             ");
        Console.WriteLine("----------------------");
        Console.WriteLine("\nEnter your choice:  ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                Dictionary dictionary = new Dictionary();
                Console.WriteLine("Enter english word: ");
                dictionary.English = Console.ReadLine();
                Console.WriteLine("Enter vietnamese word: ");
                dictionary.Vietnamese = Console.ReadLine();
                dictionaryManager.Add(dictionary);
                break;
            case 2:
                Console.WriteLine("Enter a word to find: ");
                var searching = Console.ReadLine();
                var checkSearch = dictionaryManager.Search(searching);
                if (checkSearch is false)
                {
                    Console.WriteLine("Word does not exist!");
                }
                break;
            case 3:
                Console.WriteLine("Enter a word to remove: ");
                var removing = Console.ReadLine();
                var checkRemove = dictionaryManager.Remove(removing);
                if (checkRemove is true)
                {
                    throw new Exception("Remove word success!");
                }
                else
                {
                    throw new Exception("Word does not exist!");
                }
                break;
            case 4:
                Console.WriteLine("\n-----List of word-----");
                dictionaryManager.Print();
                break;
            case 5:
                Console.WriteLine("Good bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);