﻿int[] numbers = { -1, 0, 2, -3 };
Action<int> act = Print;
PrintNumbers(numbers, act);

void PrintNumbers(int[] numbers, Action<int> action)
{
    foreach (int number in numbers)
    {
        action(number);
    }
}

void Print(int number)
{
    if (number < 0)
    {
        Console.WriteLine("Negative");
    }
    else if (number > 1)
    {
        Console.WriteLine("Positive");
    }
    Console.WriteLine("Zero");
}