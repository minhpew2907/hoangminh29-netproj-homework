﻿Console.WriteLine("Enter your number: ");
int SumEvenNumbers (int N)
{
    int sum = 0;
    for (int i = 0; i <= N; i++)
    {
        if(i % 2 == 0)
        {
            sum += i;
        }
    }
    return sum;
}
int myInput = int.Parse(Console.ReadLine());
Console.WriteLine("Result form 1-N = " + SumEvenNumbers(myInput));