﻿Console.WriteLine("Enter a number: ");
int number, nextNumb;
int firstNumb = 0;
int secondNumb = 1;
number = int.Parse(Console.ReadLine());
Console.Write("Fibonacci number: " + firstNumb + " " + secondNumb + " ");
for (int i = 2; i < number; i++)
{
    nextNumb = firstNumb + secondNumb;
    Console.Write(nextNumb + " ");
    firstNumb = secondNumb;
    secondNumb = nextNumb;
}