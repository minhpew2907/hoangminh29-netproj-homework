namespace PaperScissorTest
{
    public class UnitTest1
    {
        [Fact]
        public void PaperScissorRule_ShouldReturnPLAYER_1_WIN_WhenParamIs1AndParamIs3()
        {
            Game game = new Game();
            var param1 = 1;
            var param2 = 3;
            var expected = 1;
            var actual = game.PaperScissorRule(param1, param2);
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void PaperScissorRule_ShouldReturnPLAYER_1_WIN_WhenParamIs2AndParamIs1()
        {
            Game game = new Game();
            var param1 = 2;
            var param2 = 1;
            var expected = 1;
            var actual = game.PaperScissorRule(param1, param2);
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void PaperScissorRule_ShouldReturnPLAYER_1_WIN_WhenParamIs3AndParamIs2()
        {
            Game game = new Game();
            var param1 = 3;
            var param2 = 2;
            var expected = 1;
            var actual = game.PaperScissorRule(param1, param2);
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void PaperScissorRule_ShouldReturnPLAYER_2_WIN_WhenParamIs1AndParamIs3()
        {
            Game game = new Game();
            var param1 = 3;
            var param2 = 1;
            var expected = 2;
            var actual = game.PaperScissorRule(param1, param2);
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void PaperScissorRule_ShouldReturnPLAYER_2_WIN_WhenParamIs2AndParamIs1()
        {
            Game game = new Game();
            var param1 = 1;
            var param2 = 2;
            var expected = 2;
            var actual = game.PaperScissorRule(param1, param2);
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void PaperScissorRule_ShouldReturnPLAYER_2_WIN_WhenParamIs3AndParamIs2()
        {
            Game game = new Game();
            var param1 = 2;
            var param2 = 3;
            var expected = 2;
            var actual = game.PaperScissorRule(param1, param2);
            Assert.Equal(expected, actual);
        }
    }
}