﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1_DaoChuoi
{
    public static class DaoChuoi
    {
        public static string UpSideDown(this string ReString)
        {
            char[] arrOfString = ReString.ToCharArray();
            Array.Reverse(arrOfString);
            return string.Join(" ", arrOfString);
        }
    }
}
