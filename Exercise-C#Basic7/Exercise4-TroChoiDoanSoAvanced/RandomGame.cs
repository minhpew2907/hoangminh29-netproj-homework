﻿
class RandomGame
{
    public static string playerName;
    public static int rankedGame = 0;
    public int RandomOrg()
    {
        Random rd = new Random();
        int result = rd.Next(0, 99);
        return result;
    }
    public void Input()
    {
        Console.WriteLine("Enter your name: ");
        playerName = Console.ReadLine();

        var rdNumber = RandomOrg();
        Console.WriteLine(rdNumber);
        Console.WriteLine("Enter number in range [0 to 99]");
        int guessNumb = 0;
        do
        {
            Console.WriteLine("Enter your guess number: ");
            guessNumb = Convert.ToInt32(Console.ReadLine());
            if (guessNumb < 0)
            {
                throw new Exception("Invalid input!");
            }

            if (guessNumb < rdNumber)
            {
                Console.WriteLine("Please guess higher!");
                rankedGame++;
            }
            if (guessNumb > rdNumber)
            {
                Console.WriteLine("Please guess lower!");
                rankedGame++;
            }
            else
            {
                Console.WriteLine("Congrate you guess right!");
                rankedGame++;
            }
        } while (guessNumb != rdNumber);
    }

    public void Menu()
    {
        Console.WriteLine("-----------Random Menu-----------");
        Console.WriteLine("| 1. Play.                      |");
        Console.WriteLine("| 2. View player name & ranked. |");
        Console.WriteLine("| 3. Exit.                      |");
        Console.WriteLine("---------------------------------");
        Console.WriteLine("Enter your choice: ");
    }
}