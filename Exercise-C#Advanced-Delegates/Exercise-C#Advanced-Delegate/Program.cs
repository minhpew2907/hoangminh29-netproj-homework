﻿

using Exercise_C_Acvanced_Delegate;

SignatureManager signature = new SignatureManager();

int choice = 0;
do
{
    try
    {
        Console.WriteLine("---------Math--------");
        Console.WriteLine(" 1. Addition.        ");
        Console.WriteLine(" 2. Substraction.    ");
        Console.WriteLine(" 3. Multiplication.  ");
        Console.WriteLine(" 4. Division.        ");
        Console.WriteLine(" 5. Chia lay du.     ");
        Console.WriteLine(" 6. Exit.            ");
        Console.WriteLine("---------------------");
        Console.WriteLine("\nEnter your choice: ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                signature.AdditionNumber();
                break;
            case 2:
                signature.SubtractionNumber();
                break;
            case 3:
                signature.MultiplicationNumber();
                break;
            case 4:
                signature.DivisionNumber();
                break;
            case 5:
                signature.ChiaLayDuNumber();
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);


