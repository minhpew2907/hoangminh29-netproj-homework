use master 
go
drop database if exists LibraryManagement
go
create database LibraryManagement;
go	
use LibraryManagement;
go

create table Books
(
	BookID int primary key identity (1, 1),
	Title varchar(255) not null,
	Author varchar(255) not null,
	YearPublished int,
	Quantity int not null
);

create table BorrowerCards
(
	CardID int primary key identity(1, 1),
	MemberName varchar(100) not null,
	RegistrationDate date
);

create table Borrowings
(
	BorrowID int primary key identity(1, 1),
	CardID int foreign key references BorrowerCards(CardID),
	BookID int foreign key references Books(BookID),
	BorrowDate date,
	DueDate date 
);

drop table Borrowings; 
