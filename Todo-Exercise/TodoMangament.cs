﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Todo_Exercise
{
    public class TodoMangament
    {
        List<Todo> todoList = new List<Todo>();
        HttpClient httpClient = new HttpClient();
        const string apiUrlKey = "https://64c72d9a0a25021fde922211.mockapi.io/api/todo/Todo";

        public void ViewTaskList()
        {
            Console.WriteLine("---------Job list---------");
            foreach (Todo item in todoList)
            {
                Console.WriteLine($" Job Id: {item.JobId}" +
                          $" Job Name: {item.JobName}," +
                          $" Date Create: {item.DateCreate}," +
                          $" Completed Job: {item.IsCompleted}, " +
                          $" Priority Job: {item.Priority}");
            }
            Console.WriteLine("--------------------------");
        }

        public async Task GetData()
        {
            HttpResponseMessage response = await httpClient.GetAsync(apiUrlKey);
            var content = await response.Content.ReadAsStringAsync();
            todoList = JsonConvert.DeserializeObject<List<Todo>>(content);
        }

        public string ViewDateCreateTask()
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<Todo> temp = new List<Todo>();
            Todo todo = new Todo();
            foreach (var item in todoList)
            {
                temp.Add(item);
            }
            temp.Sort((x, y) => x.DateCreate.CompareTo(y.DateCreate));
            foreach (Todo item in temp)
            {
                stringBuilder.Append($"{item.JobName} + {item.DateCreate} + {item.DateComplete}\n");
            }
            return stringBuilder.ToString();
        }

        public void ViewPriorityTask()
        {

        }

        public async Task AddNewTask(Todo item)
        {
            await httpClient.PostAsJsonAsync(apiUrlKey, item);
        }

        public async Task RemoveTask(int taskId)
        {
            await httpClient.DeleteAsync(apiUrlKey + $"/{taskId}");
        }

        public async Task TaskCheckingComplete(int taskId)
        {
            var checkUpadate = todoList.Find(todo => todo.JobId == taskId);
            if (checkUpadate is null)
            {
                throw new Exception("Cannot checking, because id not match!");
            }
            checkUpadate.IsCompleted = true;
            checkUpadate.DateComplete = DateTime.Now;
            await httpClient.PatchAsJsonAsync(apiUrlKey + $"/{taskId}", checkUpadate);
        }

        public async Task UpgradePriorityTask(int taskId)
        {
            var upgradeToPriority = todoList.Find(todo => todo.JobId == taskId);
            if (upgradeToPriority is null)
            {
                throw new Exception("Cannot set priority, because id not match!");
            }
            Console.WriteLine("Set priority for id: "); 
            upgradeToPriority.Priority = int.Parse(Console.ReadLine());
            await httpClient.PutAsJsonAsync(apiUrlKey + $"/{taskId}", upgradeToPriority);
        }
    }
}
