﻿
Game game = new Game();
game.TurnGoldToCoin();
game.Playing();

class Game
{
    const int NUMBER_SALE_LEVEL1 = 10;
    const int NUMBER_SALE_LEVEL1_PRICE = 10;
    const int NUMBER_SALE_LEVEL2 = 5;
    const int NUMBER_SALE_LEVEL2_PRICE = 5;
    const int NUMBER_SALE_LEVEL3 = 3;
    const int NUMBER_SALE_LEVEL3_PRICE = 2;
    const int NUMBER_SALE_LEVEL_REMAIN = 1;
    const int SCISSORS = 1;
    const int ROCK = 2;
    const int PAPER = 3;
    const int TIE = 0;
    const int PLAYER1_WIN = 1;
    const int PLAYER2_WIN = 2;


    int CheckGoldToCoin(int goldAcess)
    {
        int firstGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL1);
        int price10gold = firstGold * NUMBER_SALE_LEVEL1_PRICE;
        goldAcess -= firstGold;
        Console.WriteLine("First coin recived: " + firstGold);

        int secondGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL2);
        int price5gold = secondGold * NUMBER_SALE_LEVEL2_PRICE;
        goldAcess -= secondGold;
        Console.WriteLine("Second coin recived: " + secondGold);

        int thirdGold = Math.Min(goldAcess, NUMBER_SALE_LEVEL3);
        int price3gold = thirdGold * NUMBER_SALE_LEVEL3_PRICE;
        goldAcess -= thirdGold;
        Console.WriteLine("Third coin recived: " + thirdGold);

        int lastPrice = goldAcess * NUMBER_SALE_LEVEL_REMAIN;
        Console.WriteLine("Last coin recieved: " + goldAcess);

        int totalprice = (price10gold + price5gold + price3gold + lastPrice);

        return totalprice;
    }
    public void TurnGoldToCoin()
    {
        Console.Write("Nhap so quang ban thu duoc: ");
        int goldAcess = Convert.ToInt32(Console.ReadLine());
        var NumberOfCoin = CheckGoldToCoin(goldAcess);
        Console.WriteLine("Total: " + NumberOfCoin);
    }

    int PaperScisssorRule(int player1, int player2)
    {
        if (player1 == SCISSORS && player2 == PAPER ||
            player1 == ROCK && player2 == SCISSORS ||
            player1 == PAPER && player2 == ROCK)
        {
            return PLAYER1_WIN;
        }
        if (player2 == SCISSORS && player1 == PAPER ||
            player2 == ROCK && player1 == SCISSORS ||
            player2 == PAPER && player1 == ROCK)
        {
            return PLAYER2_WIN;
        }
        return TIE;
    }
    int CheckResult(int result)
    {
        if (result == TIE)
        {
            Console.WriteLine("Tie");
        }
        else if (result == PLAYER1_WIN)
        {
            Console.WriteLine("Player 1 win");
        }
        else if (result == PLAYER2_WIN)
        {
            Console.WriteLine("Player 2 win");
        }
        return result;
    }
    public void ThingDisplay()
    {
        Console.WriteLine("1. SCISSOR.");
        Console.WriteLine("2. ROCK.");
        Console.WriteLine("3. PAPER.");
    }
    public void Playing()
    {
        ThingDisplay();
        int player1Choice = 0;
        int player2Choice = 0;
        Console.WriteLine("Player 1 choose: ");
        player1Choice = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Player 2 choose: ");
        player2Choice = Convert.ToInt32(Console.ReadLine());

        int checkResult = PaperScisssorRule(player1Choice, player2Choice);
        Console.WriteLine(CheckResult(checkResult));
    }
}