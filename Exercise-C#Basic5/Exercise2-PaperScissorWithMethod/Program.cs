﻿int choice = 0;
int Plays(int player1Choice, int player2Choice)
{
    int SCISSORS = 1;
    int ROCK = 2;
    int PAPER = 3;

    bool player1Win = player1Choice == SCISSORS && player2Choice == PAPER ||
                      player1Choice == ROCK && player2Choice == SCISSORS ||
                      player1Choice == PAPER && player2Choice == ROCK;

    if (player1Win)
    {
        return 1;
    }
    bool player2Win = player2Choice == SCISSORS && player1Choice == PAPER ||
                      player2Choice == ROCK && player1Choice == SCISSORS ||
                      player2Choice == PAPER && player1Choice == ROCK;
    if (player2Win)
    {
        return 2;
    }
    bool Tie = player1Choice == player2Choice;

    if (Tie)
    {
        return 0;
    }
    throw new Exception("not found!");
}
do
{
    Console.WriteLine("*****Welcome To Paper Scissors*****");
    Console.WriteLine("* 1. Play.                        *");
    Console.WriteLine("* 2. Exit.                        *");
    Console.WriteLine("***********************************");
    Console.WriteLine("\nEnter your choice: ");
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            Console.WriteLine("1. SCISSOR.");
            Console.WriteLine("2. ROCK.");
            Console.WriteLine("3. PAPER.");
            int player1In = 0;
            int player2In = 0;
            Console.WriteLine("Player 1 choose: ");
            player1In = int.Parse(Console.ReadLine());
            Console.WriteLine("Player 2 choose: ");
            player2In = int.Parse(Console.ReadLine()); 
            int result = Plays(player1In, player2In);
            if (result == 0)
            {
                Console.WriteLine("Tie");
            }
            else if (result == 1)
            {
                Console.WriteLine("Player 1 win");
            }
            else if (result == 2)
            {
                Console.WriteLine("Player 2 win");
            }
            break;
        case 2:
            Console.WriteLine("Thank You for playing!");
            break;
        default:
            break;
    }
} while (choice != 2);