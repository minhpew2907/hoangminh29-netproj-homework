﻿const int SCISSORS = 1;
const int ROCK = 2;
const int PAPER = 3;
const int LIZARD = 4;
const int SPOCK = 5;

Console.WriteLine("Player 1 choose: ");
int player1 = int.Parse(Console.ReadLine());
Console.WriteLine("Player 2 choose: ");
int player2 = int.Parse(Console.ReadLine());
if (player1 == SCISSORS && player2 == PAPER)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == PAPER && player2 == ROCK)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == ROCK && player2 == LIZARD)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == LIZARD && player2 == SPOCK)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == SPOCK && player2 == SCISSORS)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == SCISSORS && player2 == LIZARD)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == LIZARD && player2 == PAPER)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == PAPER && player2 == SPOCK)
{
    Console.WriteLine("Player 1 win");
}
else if (player1 == SPOCK && player2 == ROCK)
{
    Console.WriteLine("Player 1 win");
}

//-------------------------------------
else if (player2 == SCISSORS && player1 == PAPER)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == PAPER && player1 == ROCK)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == ROCK && player1 == LIZARD)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == LIZARD && player1 == SPOCK)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == SPOCK && player1 == SCISSORS)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == SCISSORS && player1 == LIZARD)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == LIZARD && player1 == PAPER)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == PAPER && player1 == SPOCK)
{
    Console.WriteLine("Player 2 win");
}
else if (player2 == SPOCK && player1 == ROCK)
{
    Console.WriteLine("Player 2 win");
}
else if ((player1 == SCISSORS && player2 == SCISSORS)
    || (player1 == ROCK && player2 == ROCK)
    || (player1 == PAPER && player2 == PAPER)
    || (player1 == LIZARD && player2 == LIZARD)
    || (player1 == SPOCK && player2 == SPOCK))
{
    Console.WriteLine("Tie");
}

