﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_EFCore_EmployeeManagement
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        
        public int Age { get; set; }
        
        public string Major { get; set; }

        public string Gender { get; set; }  
    }
}
