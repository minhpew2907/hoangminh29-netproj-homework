﻿USE MASTER
GO
DROP DATABASE IF EXISTS LECTURE4_CLASSPRACTICE
GO
CREATE DATABASE LECTURE4_CLASSPRACTICE
GO
USE LECTURE4_CLASSPRACTICE
GO
CREATE TABLE CustomerOrder(
	ID int PRIMARY KEY IDENTITY(1, 1),
	CustomerID int,
	FullName nvarchar(20) NOT NULL,
	DeliveryCity nvarchar(20),
	DateDelivery date,
	OrderDate date,
	TotalAmount money,
	OrderStatus nvarchar(20)
);

INSERT INTO CustomerOrder(CustomerID, FullName, DeliveryCity, OrderDate, DateDelivery, TotalAmount, OrderStatus)
VALUES(1, N'Ngô Kiến Huy', 'TP. HCM', '3/15/2022', '3/15/2022', 50000, 'COMPLETED'),
(1, N'Ngô Kiến Huy', 'TP. HCM', '3/12/2022', '3/25/2022', 70000, 'ON-GOING'),
(1, N'Ngô Kiến Huy', 'TP. HCM', '3/16/2022', '3/27/2022', 80000, 'CANCLED'),
(1, N'Ngô Kiến Huy', 'TP. HCM', '3/16/2022', '3/27/2022', 80000, 'COMPLETED'),
(2, N'Phan Mạnh Quỳnh', 'TP. HA NOI', '7/13/2022', '3/15/2023', 90000, 'CANCLED'),
(2, N'Phan Mạnh Quỳnh', 'TP. HA NOI', '8/12/2022', '3/25/2023', 40000, 'ON-GOING'),
(2, N'Phan Mạnh Quỳnh', 'TP. HA NOI', '9/18/2022', '3/27/2023', 30000, 'COMPLETED');

SELECT * FROM CustomerOrder;

-- PRACTICE SQL BUILT-IN FUNCTION - GROUP BY - HAVING
-- 1. IN RA THÔNG TIN ĐƠN HÀNG CÓ GIÁ TRỊ LỚN NHẤT MÀ ĐÃ HOÀN THÀNH TRONG NĂM 2022
SELECT c.FullName,c.DeliveryCity ,c.OrderStatus, MAX(TotalAmount) AS 'Greatest amount'
FROM CustomerOrder c
WHERE YEAR(DateDelivery) = 2022 and c.OrderStatus = 'COMPLETED'
GROUP BY c.FullName, c.DeliveryCity, c.OrderStatus
-- 2. IN RA THÔNG TIN ĐƠN HÀNG CÓ GIÁ TRỊ LỚN NHẤT MÀ ĐÃ BỊ HỦY TRONG NĂM 2023
SELECT c.FullName, c.DeliveryCity, c.OrderStatus, MAX(TotalAmount) AS 'Greatest amount'
FROM CustomerOrder c
WHERE YEAR(DateDelivery) = 2023 AND c.OrderStatus = 'CANCLED'
GROUP BY c.FullName, c.DeliveryCity, c.OrderStatus
-- 3. TÍNH SỐ ĐƠN HÀNG ĐƯỢC ĐẶT VÀO TRONG KHOẢNG THÁNG 7 và tháng 8 2022
SELECT OrderDate, Count(*) as 'OrderAmount'
FROM CustomerOrder
WHERE Month(OrderDate) IN (7,8) AND YEAR(OrderDate) = 2022
GROUP BY OrderDate
-- 4. TÍNH TỔNG SỐ ĐƠN HÀNG ĐÃ HOÀN THÀNH TRONG NĂM 2022 VÀ 2023
SELECT OrderDate, COUNT (*)
FROM CustomerOrder 
WHERE YEAR(OrderDate) IN (2022, 2023)
GROUP BY OrderDate
-- 5. TÍNH GIÁ TRỊ TRUNG BÌNH ĐƠN HÀNG TRONG NĂM 2023
SELECT DateDelivery, AVG(TotalAmount) AS 'AVG amount'
FROM CustomerOrder 
WHERE YEAR(DateDelivery) IN (2023)
GROUP BY DateDelivery
-- 6. IN RA THÔNG TIN ĐƠN HÀNG CÓ GIÁ TRỊ NHỎ NHẤT MÀ ĐÃ HOÀN THÀNH TRONG NĂM 2022
SELECT c.FullName, c.DeliveryCity , MIN(TotalAmount) AS 'Lowest amount'
FROM CustomerOrder c
WHERE YEAR(DateDelivery) = 2022 AND c.OrderStatus = 'COMPLETED'
GROUP BY c.FullName, c.DeliveryCity
-- 7. IN RA THÔNG TIN ĐƠN HÀNG CÓ GIÁ TRỊ NHỎ MÀ ĐÃ BỊ HỦY TRONG NĂM 2023
SELECT c.FullName, c.DeliveryCity , MIN(TotalAmount) AS 'Lowest amount'
FROM CustomerOrder c
WHERE YEAR(DateDelivery) = 2023 AND c.OrderStatus = 'CANCLED'
GROUP BY c.FullName, c.DeliveryCity
-- 8. TÍNH SỐ ĐƠN HÀNG ĐƯỢC ĐẶT THEO TỪNG THÁNG
SELECT MONTH(OrderDate) AS 'Month', COUNT(*) AS 'Order-On-Going'
FROM CustomerOrder 
GROUP BY MONTH(OrderDate)

-- 9. TÍNH SỐ ĐƠN HÀNG ĐƯỢC ĐẶT THEO TỪNG NĂM
SELECT YEAR(OrderDate) AS 'Year', COUNT(*) AS 'Order-On-Going'
FROM CustomerOrder 
GROUP BY YEAR(OrderDate)
-- 10. TÍNH GIÁ TRỊ TRUNG BÌNH ĐƠN HÀNG THEO TỪNG THÁNG TRONG NĂM 2023
SELECT OrderDate, AVG(TotalAmount)
FROM CustomerOrder
WHERE YEAR(OrderDate) IN (2022, 2023)
GROUP BY OrderDate

-- 11. TỔNG SỐ ĐƠN HÀNG ĐƯỢC ĐẶT VÀO THÁNG 3 NĂM 2022
SELECT OrderDate, SUM(TotalAmount) AS 'Sum Of Order'
FROM CustomerOrder
WHERE MONTH(OrderDate) = 3 AND YEAR(OrderDate) = 2022
GROUP BY OrderDate
-- 12. TÍNH TỔNG SỐ ĐƠN HÀNG THEO TỪNG KHÁCH HÀNG
SELECT CustomerID, FullName, COUNT(*) AS 'Sum Of Order In Months'
FROM CustomerOrder
GROUP BY CustomerID, FullName
-- 13. LẤY THÔNG TIN KHÁCH HÀNG ĐẶT TỪ 2 ĐƠN HÀNG TRỞ LÊN
SELECT CustomerID, FullName, COUNT(*) AS 'Number Of Order'
FROM CustomerOrder 
GROUP BY CustomerID, FullName
HAVING COUNT(*) > 2
-- 14. TÍNH SỐ ĐƠN HÀNG THEO THÁNG VÀ PHẢI CÓ ÍT NHẤT 2 ĐƠN HÀNG TRỞ LÊN
SELECT CustomerID, FullName, COUNT(*) AS 'Number Of Order In Months'
FROM CustomerOrder 
GROUP BY CustomerID, FullName
HAVING COUNT(*) > 0 AND COUNT(*) > 2
-- 15. LẤY TÊN KHÁCH HÀNG CÓ GIÁ TRỊ ĐƠN HÀNG TRUNG BÌNH LỚN HƠN 6000 TRÊN 1 ĐƠN HÀNG VÀ CÓ ÍT NHẤT 2 ĐƠN HÀNG TRỞ LÊN
SELECT CustomerID, FullName, AVG(TotalAmount) AS 'AVG Order', COUNT(*) AS 'Number Of Order' 
FROM CustomerOrder
GROUP BY CustomerID, FullName
HAVING AVG(TotalAmount) > 6000 AND COUNT(*) > 2
-- 16. TÍNH SỐ NGÀY TRUNG BÌNH ĐỂ GIAO 1 ĐƠN HÀNG CHO THEO KHÁCH HÀNG CHO NHỮNG KHÁCH ĐẶT TỪ 2 ĐƠN TRỞ LÊN
SELECT CustomerID, Fullname, AVG(DATEDIFF(DAY, OrderDate, DateDelivery)) AS 'Date to delivery', COUNT(*) AS 'Number Of Order'
FROM CustomerOrder
GROUP BY CustomerID, FullName
HAVING COUNT(*) > 2
-- 17. TÍNH TỔNG ĐƠN HÀNG THEO THÀNH PHỐ VÀ CHỈ HIỂN THỊ THÀNH PHỐ CÓ TRÊN 2 ĐƠN HÀNG
SELECT DeliveryCity, COUNT(*) AS 'Number Of Order'
FROM CustomerOrder
GROUP BY DeliveryCity
HAVING COUNT(*) > 2
-- 18. TÍNH TỔNG ĐƠN HÀNG ĐÃ HOÀN THÀNH THEO THÀNH PHỐ VÀ CHỈ HIỂN THỊ THÀNH PHỐ CÓ SỐ ĐƠN HÀNG HOÀN THÀNH TỪ 2 ĐƠN TRỞ LÊN
SELECT DeliveryCity, OrderStatus, COUNT(*) AS 'Number Of Order'
FROM CustomerOrder
WHERE OrderStatus = 'COMPLETED'
GROUP BY DeliveryCity, OrderStatus
HAVING COUNT(*) < 2
-- 19. TÍNH TỔNG ĐƠN HÀNG ĐÃ BỊ HỦY THEO THÀNH PHỐ VÀ CHỈ HIỂN THỊ THÀNH PHỐ CÓ SỐ ĐƠN HÀNG BỊ HỦY TỪ 2 ĐƠN TRỞ LÊN
SELECT DeliveryCity, OrderStatus, COUNT(*) AS 'Number Of Order'
FROM CustomerOrder
WHERE OrderStatus = 'CANCLED'
GROUP BY DeliveryCity, OrderStatus
HAVING COUNT(*) < 2
-- lưu nội dung câu truy vấn vô file text(.txt) rồi nộp lại nha