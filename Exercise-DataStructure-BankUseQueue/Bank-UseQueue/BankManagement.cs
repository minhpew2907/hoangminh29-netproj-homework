﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Bank_UseQueue
{
    public class BankManagement
    {
        public Bank[] banks;
        int maxQueue;
        int front;
        int rear = -1;
        const int MOT_TY = 1_000_000_000;

        public BankManagement(int maxQueue)
        {
            this.maxQueue = maxQueue;
            banks = new Bank[maxQueue];
            front = 0;
        }

        public void CustomerFillIn(Bank bank)
        {
            Console.WriteLine("Enter your customer name: ");
            bank.CustomerName = Console.ReadLine();
            Console.WriteLine("Enter your money withdraw: ");
            bank.MoneyWithdraw = int.Parse(Console.ReadLine());
        }

        public void AddCustomer(Bank bank)
        {
            if (isFull())
            {
                throw new Exception("Queue is Full");
            }
            rear++;
            banks[rear] = bank;
            if (bank.MoneyWithdraw >= MOT_TY)
            {
                Console.WriteLine("You are the VIP PRO customer");
                for (int i = rear; i > front; i--)
                {
                    banks[i] = banks[i - 1];
                }
                banks[front] = bank;
            }
            banks[rear] = bank;
        }

        public Bank CallNextCustomer()
        {
            if (isEmpty())
            {
                throw new Exception("Queue is empty");
            }
            var valueAtFront = banks[front];
            if (front == rear)
            {
                front = 0;
                rear = -1;
            }
            else
            {
                front++;
            }
            return valueAtFront;
        }

        public string ViewThreeCustomer()
        {
            if (isEmpty())
            {
                throw new Exception("queue is empty");
            }
            StringBuilder sb = new StringBuilder();
            int bruh = Math.Min(front + 3, rear + 1);
            for (int i = front; i < bruh; i++)
            {
                sb.Append($"\nCustomer name: {banks[i].CustomerName}  \nMoney Withdraw: {banks[i].MoneyWithdraw}");
            }
            return sb.ToString();
        }


        public bool isVipPro(Bank bank) => bank.MoneyWithdraw >= MOT_TY;
        public bool isEmpty() => front > rear;
        public bool isFull() => (rear - front + 1 >= maxQueue);
        public int ElementCount() => rear - front + 1;
    }
}
