﻿

namespace Exercise_Encapsulation
{
    public class Book
    {
        public int _Id { get; set; }
        public string _Title { get; set; }
        public string _Author { get; set; }
        public bool _Available { get; set; }

        public Book()
        {
        }

        public Book(int id, string title, string author, bool available)
        {
            _Id = id;
            _Title = title;
            _Author = author;
            _Available = true;
        }
    }
}
