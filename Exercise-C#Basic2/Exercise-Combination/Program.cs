﻿//Kiem tra positive, negative, zero
Console.WriteLine("Enter a number: ");
int number = int.Parse(Console.ReadLine());
if (number > 0)
{
    Console.WriteLine("Positive number");
}
else if (number < 0)
{
    Console.WriteLine("Negative number");
}
else
{
    Console.WriteLine("Number is equal 0");
}