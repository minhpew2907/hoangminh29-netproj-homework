﻿using System.Diagnostics;

int[] numbers = { -1, 0, 2, -3, 4, 0, -5 };
Func<int, int> func = MyFunc;
int[] results = ProcessNumbers(numbers, func);

foreach (int result in results)
{
    Console.Write(result + " ");
}

int[] ProcessNumbers(int[] numbers, Func<int, int> func)
{
    int[] result = new int[numbers.Length];
    for (int i = 0; i < numbers.Length; i++)
    {
        result[i] = func(numbers[i]);
    }
    return result;
}

int MyFunc(int x)
{
    if (x > 0)
    {
        return x * x;
    }
    else if (x < 0)
    {
        return Math.Abs(x);
    }
    return 0;
}