﻿//Mang 1 chieu
int[] arrNumber =
    { 9,6,7,5,3,8,4};
int largestNumber = arrNumber[0];
int smallestNumber = arrNumber[0];
for (int i = 0; i < arrNumber.Length; i++)
{
    if (largestNumber < arrNumber[i])
    {
        largestNumber = arrNumber[i];
    }
}
Console.WriteLine("Gia tri lon nhat: " + largestNumber);
for (int j = 0; j < arrNumber.Length; j++)
{
    if (smallestNumber > arrNumber[j])
    {
        smallestNumber = arrNumber[j];
    }
}
Console.WriteLine("Gia tri nho nhat: " + smallestNumber);

//Mang 2 chieu
int[,] arrNumber2 =
{
     { 9,6,7,5,3,8,4},
     {10,12,11,20,12,13,14}
};
int largestNumber2 = arrNumber2[0, 0];
int smallestNumber2 = arrNumber2[0, 0];
for (int i = 0; i < arrNumber2.GetLength(0); i++)
{
    for (int j = 0; j < arrNumber2.GetLength(1); j++)
    {
        if (largestNumber2 < arrNumber2[i, j])
        {
            largestNumber2 = arrNumber2[i, j];
        }
    }
}
Console.WriteLine("Gia tri lon nhat: " + largestNumber2);
for (int k = 0; k < arrNumber2.GetLength(0); k++)
{
    for (int l = 0; l < arrNumber2.GetLength(1); l++)
    {
        if (smallestNumber2 > arrNumber2[k, l])
        {
            smallestNumber2 = arrNumber2[k, l];
        }
    }
}
Console.WriteLine("Gia tri nho nhat: " + smallestNumber2);