﻿int num = 10;
int add = 5;
int sub = 3;
int mult = 2;
int div = 4;
int mod = 3;
Console.WriteLine("Gia tri cua num before: " + num + "\nGia tri cua num after: " + (num += add));
Console.WriteLine("Gia tri cua num before: " + num + "\nGia tri cua num after: " + (num -= sub));
Console.WriteLine("Gia tri cua num before: " + num + "\nGia tri cua num after: " + (num *= mult));
Console.WriteLine("Gia tri cua num before: " + num + "\nGia tri cua num after: " + (num /= div));
Console.WriteLine("Gia tri cua num before: " + num + "\nGia tri cua num after: " + (num %= mod));

