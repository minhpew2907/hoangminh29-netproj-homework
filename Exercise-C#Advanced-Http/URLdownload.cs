﻿

namespace Exercise_C_Advanced_Http
{
    public class URLdownload
    {
        List<string> urlList = new List<string>();
        string dircectoryPath = @"Directory";
        HttpClient httpClient = new HttpClient();

        public void GetUrl()
        {
            Console.WriteLine("Enter your url: ");
            string url = Console.ReadLine();
            if (url.Length < 0)
            {
                throw new Exception("Get not success!");
            }
            urlList.Add(url);
        }

        public async Task DownloadImage()
        {
            if (!Directory.Exists(dircectoryPath))
            {
                Directory.CreateDirectory(dircectoryPath);
            }
            if(urlList.Count == 0)
            {
                throw new Exception("List is empty!");
            }
            
            foreach (string url in urlList)
            {
                var data = await httpClient.GetByteArrayAsync(url);
                string name = Path.GetFileName(url);
                string fileLocation = string.Concat(dircectoryPath, $"/{name}");
                await File.WriteAllBytesAsync(fileLocation, data);
            }
        }
    }
}
