﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_C_Avanced_Generic_Delegate_Workout
{
    public class WorkoutManager : DelegateWorkout<int>
    {
        protected override int PerformOperation(int param1, int param2, MathOperation operation)
        {
            return base.PerformOperation(param1, param2, operation);
        }

        private Func<int, int, int> Addition = (a, b) => a + b;
        private Func<int, int, int> Substraction = (a, b) => a - b;
        private Func<int, int, int> Multiplication = (a, b) => a * b;
        private Func<int, int, int> Divsion = (a, b) => a / b;
        //private Func<double, double, double> AdditionDouble = (a, b) => a + b;
        //private Func<float, float, float> MultiplicationFloat = (af, bf) => af * bf;
        //private Func<double, double, double> AddTwoString = (stra, strb) => stra / strb;
        private int x;
        private int y;

        public void Input()
        {
            Console.WriteLine("Enter x: ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y: ");
            y = int.Parse(Console.ReadLine());
        }

        public void AdditionInt()
        {
            Input();
            var addition = new MathOperation(Addition);
            var result = PerformOperation(x, y, addition);
            Console.WriteLine(result);
        }
        public void SubstractionInt()
        {
            Input();
            var substraction = new MathOperation(Substraction);
            var result = PerformOperation(x, y, substraction);
            Console.WriteLine(result);
        }
        public void MultiplicationInt()
        {
            Input();
            var multiply = new MathOperation(Multiplication);
            var result = PerformOperation(x, y, multiply);
            Console.WriteLine(result);
        }
        public void DivisionInt()
        {
            Input();
            var division = new MathOperation(Divsion);
            var result = PerformOperation(x, y, division);
            Console.WriteLine(result);
        }
    }
}
