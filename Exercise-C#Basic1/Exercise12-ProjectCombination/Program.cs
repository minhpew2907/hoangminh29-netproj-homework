﻿//Tinh Dien tich va Chu vi hinh tron
Console.WriteLine("Nhap so: ");
double pi = Math.PI;
double r = double.Parse(Console.ReadLine());

Console.WriteLine("Area: " + pi * Math.Pow(r, 2));
Console.WriteLine("Circumfernce: " + 2 * r * pi);
//Tinh Dien Tich va Chu vi hinh vuong
Console.WriteLine("Nhap canh: ");
double canh = double.Parse(Console.ReadLine());
Console.WriteLine("Area: " + canh * canh);
Console.WriteLine("Circumference: " + canh * 4);

//Tinh Dien tich va chu vi hinh tam giac
Console.WriteLine("Nhap canh 1: ");
double canh1 = double.Parse(Console.ReadLine());
Console.WriteLine("Nhap canh 2: ");
double canh2 = double.Parse(Console.ReadLine());
Console.WriteLine("Nhap canh 3: ");
double canh3 = double.Parse(Console.ReadLine());
Console.WriteLine("Area: " + (canh1 * canh2) / 2);
Console.WriteLine("Circumference: " + canh1 + canh2 + canh3);

//Tinh dien tich chu vi hinh thang
Console.WriteLine("Nhap canh a: ");
double a = double.Parse(Console.ReadLine());
Console.WriteLine("Nhap canh b: ");
double b = double.Parse(Console.ReadLine());
Console.WriteLine("Nhap chieu dai: ");
double heigth = double.Parse(Console.ReadLine());
Console.WriteLine("Area: " + (a + b) * 2);
Console.WriteLine("Circumference: " + a * heigth);
