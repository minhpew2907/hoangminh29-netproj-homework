﻿using System.Text;

int choice = 0;
Console.OutputEncoding = Encoding.UTF8;
do
{
    Console.WriteLine("$----Chào mừng đến với Random.org----$");
    Console.WriteLine("| 1. Chơi.                           |");
    Console.WriteLine("| 2. Thoát game.                     |");
    Console.WriteLine("$------------------------------------$");

    choice = int.Parse(Console.ReadLine());
    int randomNumber = Random.Shared.Next(0, 100);
    int guessNumber;
    switch (choice)
    {
        case 1:
            do
            {
                Console.WriteLine("Số dự đoán của bạn nằm trong khoảng từ 0 đến 99.");
                Console.WriteLine("Nhập số bạn muốn dự đoán: ");
                guessNumber = int.Parse(Console.ReadLine());
                if (guessNumber == randomNumber)
                {
                    Console.WriteLine("Đúng rồi cu! Xin chúc mừng");
                }
                else if (guessNumber < randomNumber)
                {
                    Console.WriteLine("Đoán lớn thêm xíu cu!");
                }
                else
                {
                    Console.WriteLine("Đoán thấp xuống chút nữa cu!");
                }
            } while (guessNumber != randomNumber);
            break;
        case 2:
            Console.WriteLine("Cảm ơn chú đã tham gia trò chơi! Hẹn ngày gặp lại");
            break;
        default:
            Console.WriteLine("Thử lại đi ba!");
            break;
    }

} while (choice != 2);
