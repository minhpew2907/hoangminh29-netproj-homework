﻿

using Exercise_C_Avanced_Generic_Delegate_Workout;

WorkoutManager workoutManager = new WorkoutManager();

int choice = 0;
do
{
    try
    {
        Console.WriteLine(" ------Workout-------");
        Console.WriteLine("| 1. Add two number |");
        Console.WriteLine("| 2. Substraction two number |");
        Console.WriteLine("| 3. Multiplication two number |");
        Console.WriteLine("| 4. Division two number |");
        Console.WriteLine("| 5. Exit           |");
        Console.WriteLine(" --------------------");
        Console.WriteLine("\nEnter your choice: ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                workoutManager.AdditionInt();
                break;
            case 2:
                workoutManager.SubstractionInt();
                break;
            case 3:
                workoutManager.MultiplicationInt();
                break;
            case 4:
                workoutManager.DivisionInt();
                break;
            default:
                Console.WriteLine("Good Bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);