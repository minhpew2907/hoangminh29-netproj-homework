﻿
Employee emp = new Employee(0, "Hoang Minh", 22, 5000.0);
Employee emp2 = new Employee(1, "Minh Pew", 23, 6000.0);
Employee emp3 = new Employee(2, "Chu 10", 24, 7000.0);
emp.DisplayEmployeeInfo();
emp2.DisplayEmployeeInfo();
emp3.DisplayEmployeeInfo();

class Employee
{
    public Employee()
    {

    }

    public Employee(int id, string name)
    {
        Id = id;
        Name = name;
    }

    public Employee(int id, string name, int age, double salary) : this(id, name)
    {
        Age = age;
        Salary = salary;
    }

    public void DisplayEmployeeInfo()
    {
        Console.WriteLine("Employee Id: " + Id);
        Console.WriteLine("Employee Name: " + Name);
        Console.WriteLine("Employee Age: " + Age);
        Console.WriteLine("Employee Salary: " + Salary);
    }

    public int Id;
    public string Name;
    public int Age;
    public double Salary;
}

