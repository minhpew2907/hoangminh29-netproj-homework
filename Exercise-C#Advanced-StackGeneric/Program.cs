﻿using Exercise_C_Avanced_StackGeneric;

var stack = new MyStack<int>(5);
int choice = 0;
do
{
    try
    {
        Console.WriteLine(" ----------Stack----------- ");
        Console.WriteLine("| 1. Push in stack.        |");
        Console.WriteLine("| 2. Pop in stack.         |");
        Console.WriteLine("| 3. Peak in stack.        |");
        Console.WriteLine("| 4. View Stack.           |");
        Console.WriteLine("| 5. Last index in stack.  |");
        Console.WriteLine("| 6. Exit.                 |");
        Console.WriteLine(" -------------------------- ");
        Console.WriteLine("\nEnter your choice: ");

        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {

            case 1:
                var pushStack = int.Parse(Console.ReadLine());
                stack.Push(pushStack);
                break;
            case 2:
                var popStack = stack.Pop();
                Console.WriteLine(popStack);
                break;
            case 3:
                var peakStack = stack.Top();
                Console.WriteLine("Peak element: " + peakStack);
                break;
            case 4:
                stack.Print();
                break;
            case 5:
                var lastItem = stack.LastItemIndex();
                Console.WriteLine("Top index: " + lastItem);
                break;
            case 6:
                Console.WriteLine("Good bye");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);