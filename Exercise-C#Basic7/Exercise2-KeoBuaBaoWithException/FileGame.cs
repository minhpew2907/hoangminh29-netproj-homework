﻿class FileGame
{
    const string FILE_PATH = @"C:\Users\ADMIN\Desktop\NET j Pro ram\Exercise-C#BasicFullCourse\Exercise-C#Basic7\Exercise2-KeoBuaBaoWithException\save.txt";

    public bool ExistFile()
    {
        if (File.Exists(FILE_PATH))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void ReadFile()
    {
        if (ExistFile())
        {
            string filecontent = File.ReadAllText(FILE_PATH);
            Console.WriteLine(filecontent);
        }
        else
        {
            throw new Exception("File not exist");
        }
    }
    public void WriteFile()
    {
        string player1Name = Game.player1Name;
        string player2Name = Game.player2Name;
        int player1Score = Game.player1Score;
        int player2Score = Game.player2Score;   
        if (ExistFile())
        {
            string content = "Player 1 name: " + player1Name + "\nPlayer 2 name: " + player2Name + "\nPlayer 1 score: " + player1Score + "\nPlayer 2 score: " + player2Score;
            File.WriteAllText(FILE_PATH, content);
        }
    }
}