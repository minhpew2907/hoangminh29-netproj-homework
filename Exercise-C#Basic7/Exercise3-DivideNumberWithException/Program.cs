﻿
Divide2Numbers divide = new Divide2Numbers();

try
{
    Console.WriteLine(divide.DivideNumbers());
}
catch (DivideByZeroException dv)
{
    Console.WriteLine(dv.Message);
}

class Divide2Numbers
{
    public double DivideNumbers()
    {
        int Numb1 = 0;
        int Numb2 = 0;
        double result = 0;
        Console.WriteLine("Enter first number: ");
        Numb1 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Enter second number: ");
        Numb2 = Convert.ToInt32(Console.ReadLine());
        if (Numb2 == 0)
        {
            throw new DivideByZeroException("Cannot divide by zero");
        }
        else
        {
            result = Numb1 / Numb2;
        }
        return result;
    }
}