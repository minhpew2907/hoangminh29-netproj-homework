﻿Console.WriteLine("Nhap a (True/False): ");
bool a = bool.Parse(Console.ReadLine());
Console.WriteLine("Nhap b (True/False): ");
bool b = bool.Parse(Console.ReadLine());
Console.WriteLine("Nhap c (True/False): ");
bool c = bool.Parse(Console.ReadLine());
//1. Nhap a = True, b = False, c = True
//3. Nhap a = False, b = True, c = True
Console.WriteLine("bool result = " + (a && (b || !c)));
//2. Nhap a = True, b = False, c = True
//4. Nhap a = False, b = True, c = True
//7. Nhap a = False, b = True, c = True
Console.WriteLine("bool result = " + (!a || (b && c)));
//5. Nhap a = True, b = False, c = False
Console.WriteLine("bool result = " + (!a && (b || c)));
//6. Nhap a = True, b = True, c = False
Console.WriteLine("bool result = " + (a && (b || c)));
//8. Nhap a = False, b = True, c = False
Console.WriteLine("bool result = " + (a || (b && !c)));
//9. Nhap a = True, b = False, c = False
Console.WriteLine("bool result = " + (!a || (b && !c)));
//10. Nhap a = False, b = True, c = True
Console.WriteLine("bool result = " + (!a && (b || !c)));