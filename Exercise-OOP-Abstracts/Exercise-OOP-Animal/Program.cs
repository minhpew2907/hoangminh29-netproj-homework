﻿
using Animal;

Animals[] animals = new Animals[6];
animals[0] = new Dog("Kiki", 8, "Dog", "Chihuahua");
animals[1] = new Dog("Von", 7, "Dog", "Pitbull");
animals[2] = new Dog("Kleevv", 8, "Dog", "Barbarian");
animals[3] = new Cat("Doki", 4, "Cat", "England");
animals[4] = new Cat("Lewandow", 5, "Cat", "Cuffin");
animals[5] = new Cat("Hoo Hoo", 8, "Cat", "LongSleeve");

foreach (var animal in animals)
{
    animal.Speak();
    if (animal is Dog dog)
    {
        dog.Bark();
    }
    if(animal is Cat cat)
    {
        cat.Meow();
    }
}






