﻿

namespace Exercise_C_Acvanced_Dictionary
{
    public class DictionaryManager
    {
        Dictionary<string, string> dictionarys = new Dictionary<string, string>();

        public void Add(Dictionary dictionary)
        {
            dictionarys.Add(dictionary.English, dictionary.Vietnamese);
        }

        public bool Search(string word)
        {
            Console.WriteLine("\n--------Word Found--------");
            foreach (var dictionary in dictionarys)
            {
                if (dictionary.Key.Equals(word) || dictionary.Value.Equals(word))
                {
                    Console.WriteLine($"{dictionary.Key} => {dictionary.Value}");
                    return true;
                }
            }
            return false;
        }

        public void EditValue(string word)
        {

        }

        public bool Remove(string word)
        {
            foreach (var dictionary in dictionarys)
            {
                if (dictionary.Key.Equals(word) || dictionary.Value.Equals(word))
                {
                    dictionarys.Remove(dictionary.Key);
                    return true;
                }
            }
            return false;
        }

        public void Print()
        {
            foreach (var dictionary in dictionarys)
            {
                Console.WriteLine($"English: {dictionary.Key} => Vietnamese: {dictionary.Value}\n");
            }
        }
    }
}
