﻿/*Console.WriteLine("Enter list of number: ");
int n = int.Parse(Console.ReadLine());
int[] arrNumber = new int[n];
int largest = arrNumber[0];
for (int i = 0; i < arrNumber.Length; i++)
{
    Console.WriteLine("Enter {0} value: ", i + 1);
    arrNumber[i] = int.Parse(Console.ReadLine());

}
for (int i = 0; i < arrNumber.Length; i++)
{
    if (largest < arrNumber[i])
    {
        largest = arrNumber[i];
    }
}
Console.WriteLine("\n\nThe largest number of list = " + largest);*/

int[,,] arrNumber =
{
   {
      { 1, 2, 3 },
      { 4, 5, 6 }
   },
   {
      {8, 9, 10 },
      {10, 33, 12}
   },
   {
      {13, 29, 15 },
      {16, 17, 18 }
   }
};
int largest = arrNumber[0,0,0];
for (int i = 0; i < arrNumber.GetLength(0); i++)
{
    for (int j = 0; j < arrNumber.GetLength(1); j++)
    {
        for (int k = 0; k < arrNumber.GetLength(2); k++)
        {
            if (largest < arrNumber[i, j, k]) { 
                largest = arrNumber[i, j, k];
            }
        }
    }
}
Console.WriteLine("\n\nThe largest number of list = " + largest);



