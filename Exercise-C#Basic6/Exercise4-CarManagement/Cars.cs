﻿using System.Threading.Channels;

class Car
{
    public string Make;
    public string Model;
    public int Year;
    public string Color;
    public Car[] arrcar = new Car[10];

    public Car()
    {

    }
    public void CarMenu()
    {
        Console.WriteLine("------Car Management------");
        Console.WriteLine("| 1. View list of Car.   |");
        Console.WriteLine("| 2. Add a new Car       |");
        Console.WriteLine("| 3. Exit.               |");
        Console.WriteLine("--------------------------");
        Console.WriteLine("\nEnter your choice: ");
    }
    public void Exist()
    {
        arrcar[0] = new Car("Ford", "Mustang", 2022, "Red");
        arrcar[1] = new Car("Toyota", "Supra MK4", 2022, "Yellow");
        arrcar[2] = new Car("BMW", "I8", 2020, "Blue");
        arrcar[3] = new Car("Honda", "Civic", 2021, "Black");
        arrcar[4] = new Car("Nissan", "SkyLine R34", 2018, "Grey");
        arrcar[5] = new Car("Mazda", "RX7", 2008, "Cyan");
        arrcar[6] = new Car("Ferrari", "F Spyder", 2018, "Red");
        arrcar[7] = new Car("Lamborghini", "Veneno", 2020, "Dark Gray");
        arrcar[8] = new Car("Toyota", "Supra MK3", 2001, "Orange");
        arrcar[9] = new Car("Roll Royce", "Phantom", 2022, "Gloss Black");
    }

    public Car(string make, string model, int year, string color)
    {
        Make = make;
        Model = model;
        Year = year;
        Color = color;
    }

    public void PrintCarInfo()
    {
        foreach (Car car in arrcar)
        {
            Console.WriteLine("Car making by: " + car.Make);
            Console.WriteLine("Car model: " + car.Model);
            Console.WriteLine("Year of manfacture: " + car.Year);
            Console.WriteLine("Car color: " + car.Color);
        }
    }
    public Car AddNewCar()
    {
        int newSizeArray = arrcar.Length + 1;
        Array.Resize(ref arrcar, newSizeArray);
        Car car = new Car();
        Console.WriteLine("Add a new car maker: ");
        car.Make = Console.ReadLine();
        Console.WriteLine("Add a car model: ");
        car.Model = Console.ReadLine();
        Console.WriteLine("Add a year of manufacture: ");
        car.Year = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Add a car color: ");
        car.Color = Console.ReadLine();
        return arrcar[newSizeArray - 1] = car;
    }
}