﻿USE MASTER
GO
DROP DATABASE IF EXISTS EmployeeManagerment
GO

CREATE DATABASE EmployeeManagerment;
GO

USE EmployeeManagerment;
GO

CREATE TABLE Departments (
    ID INT PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    ManagerId INT 
);

-- Create tables
CREATE TABLE Employees (
    Id INT PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    DepartmentId INT,
    Salary INT NOT NULL,
    ManagerId INT 
);


-- Insert data into Departments table
INSERT INTO Departments (ID, Name, ManagerId) VALUES
(1, 'Marketing', 1),
(2, 'Sales', 2),
(3, 'IT', 3),
(4, 'HR', 4),
(5, 'FUN', 0);

-- Insert data into Employees table
INSERT INTO Employees (Id, Name, DepartmentId, Salary, ManagerId) VALUES
(1, 'John', 1, 50000, 1),
(2, 'Jane', 2, 60000, 2),
(3, 'Bob', 3, 70000, 3),
(4, 'Sue', 4, 55000, 4),
(5, 'Dave', 1, 45000, 1),
(6, 'Alice', 2, 65000, 2),
(7, 'Fred', 3, 80000, 3),
(8, 'Mary', 4, 50000, 4),
(9, 'Tom', 1, 55000, 8),
(10, 'Kate', 2, 70000, 7),
(11, 'Haha', 1000, 70000, 5);

SELECT *
FROM Departments

SELECT *
FROM Employees

--INNER JOIN

--Lấy tên và phòng ban của mỗi nhân viên, cùng với tên của người quản lý phòng ban.
SELECT emp.DepartmentId, emp.Name, dp.Name, emp2.Name
FROM Employees emp
INNER JOIN Departments dp ON emp.DepartmentId = dp.ID 
INNER JOIN Employees emp2 ON emp2.ID = emp.ManagerId
--Lấy tên và lương của mỗi nhân viên, cùng với tên của phòng ban mà họ làm việc.
SELECT  emp.DepartmentId, emp.Name, emp.Salary, dp.Name
FROM Employees emp
INNER JOIN Departments dp ON emp.DepartmentId = dp.ID
--Lấy tên và lương của mỗi nhân viên làm việc trong phòng ban IT.
SELECT emp.DepartmentId, emp.Name, emp.Salary
FROM Employees emp
INNER JOIN Departments dp ON emp.DepartmentId = dp.ID
WHERE dp.Name = 'IT'
--Lấy tên của mỗi phòng ban và tổng lương của tất cả nhân viên làm việc trong phòng ban đó.
SELECT emp.DepartmentId, dp.Name, SUM(emp.Salary) AS 'Sum of salary'
FROM Employees emp
INNER JOIN Departments dp ON emp.DepartmentId = dp.ID 
GROUP BY emp.DepartmentId, dp.Name
--OUTER JOIN

--Lấy tên và phòng ban của mỗi nhân viên, cùng với tên của người quản lý phòng ban (bao gồm cả nhân viên không có người quản lý).
SELECT e.DepartmentId, d.Name, e.Name, e.ID, e2.ID
FROM Employees e
LEFT JOIN Departments d ON e.DepartmentId = d.ID
LEFT JOIN Employees e2 ON e.ManagerId = e2.ID
--Lấy tên và lương của mỗi nhân viên, cùng với tên của phòng ban mà họ làm việc (bao gồm cả nhân viên không có phòng ban).
SELECT e.DepartmentId, e.Name, e.Salary, d.Name
FROM Employees e
LEFT JOIN Departments d ON e.DepartmentId = d.ID
--Lấy tên và lương của mỗi nhân viên làm việc trong phòng ban IT (bao gồm cả nhân viên không có phòng ban).
SELECT e.DepartmentId, e.Name, e.Salary, d.Name
FROM Employees e
LEFT JOIN Departments d ON e.DepartmentId = d.ID
WHERE d.Name = 'IT'
--Lấy tên của mỗi phòng ban và tổng lương của tất cả nhân viên làm việc trong phòng ban đó (bao gồm cả phòng ban không có nhân viên).
SELECT d.Name, SUM(e.Salary) AS 'Sum of Salary'
FROM Departments d
LEFT JOIN Employees e ON e.DepartmentId = d.ID
GROUP BY d.Name

--EXCLUDING JOIN:

--Viết một câu truy vấn để lấy tất cả nhân viên không có phòng ban.
SELECT  e.Name, d.ID AS 'Phong ban'
FROM Employees e
LEFT JOIN Departments d ON d.ID = e.DepartmentId
WHERE d.ID IS NULL
--Viết một câu truy vấn để lấy tất cả phòng ban không có nhân viên.
SELECT d.ID, d.Name
FROM Departments d
LEFT JOIN Employees e ON e.DepartmentId = d.ID
WHERE e.DepartmentId IS NULL

--Viết một câu truy vấn để lấy tất cả nhân viên không có người quản lý.
SELECT *
FROM Employees e
LEFT JOIN Employees e2 ON e.ID = e2.ManagerId
WHERE e2.ManagerId IS NULL
--Viết một câu truy vấn để lấy tất cả người quản lý không có nhân viên trong phòng ban của họ.
SELECT ma.Name AS 'Manager'
FROM Employees ma 
LEFT JOIN Employees e ON e.ManagerId = ma.ID
WHERE ma.ManagerId IS NULL AND e.ID IS NULL

--SELF JOIN:

--Viết một câu truy vấn để lấy tất cả nhân viên và tên của người quản lý của họ.
SELECT e.Name AS 'Ten nhan vien', ma.Name AS 'Ten quan ly'
FROM Employees e
LEFT JOIN Employees ma ON e.ManagerId = ma.ID

--Viết một câu truy vấn để lấy tất cả nhân viên và đồng nghiệp cùng phòng ban.
SELECT e.Name AS 'Ten nhan vien', e2.Name AS 'Ten dong nghiep'
FROM Employees e
INNER JOIN Employees e2 ON e.DepartmentId = e2.DepartmentId
WHERE e.Id != e2.Id
--Viết một câu truy vấn để lấy tất cả phòng ban và tên của người quản lý.
SELECT d.Name AS 'Phong ban', e.Name AS 'Ten quan ly'
FROM Departments d
LEFT JOIN Employees e ON d.ManagerId = e.Id
--Viết một câu truy vấn để lấy tất cả phòng ban và tên của nhân viên.
SELECT d.Name AS 'Phong ban', e.Name AS 'Ten nhan vien'
FROM Departments d
LEFT JOIN Employees e ON e.DepartmentId = d.Id
--UNION và UNION ALL:

--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales sử dụng toán tử UNION.
SELECT Name AS 'Nhan vien'
FROM Employees  
WHERE DepartmentId = 1
UNION 
SELECT Name AS 'Nhan vien' 
FROM Employees 
WHERE DepartmentId = 2 

--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales sử dụng toán tử UNION ALL.
SELECT Name AS 'Nhan vien'
FROM Employees  
WHERE DepartmentId = 1 
UNION ALL
SELECT Name AS 'Nhan vien' 
FROM Employees 
WHERE DepartmentId = 2 
--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales và sắp xếp theo lương giảm dần.
SELECT Name AS 'Nhan vien', Salary
FROM Employees  
WHERE DepartmentId = 1 
UNION ALL 
SELECT Name AS 'Nhan vien', Salary 
FROM Employees 
WHERE DepartmentId = 2 
ORDER BY Salary DESC
--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales và loại bỏ bất kỳ bản sao nào sử dụng toán tử UNION.
SELECT * 
FROM Employees
WHERE DepartmentId = 1
UNION
SELECT *
FROM Employees
WHERE DepartmentId = 2