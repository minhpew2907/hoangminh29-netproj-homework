﻿Game game = new Game();
FileGame file = new FileGame();

int choice = 0;
do
{
    try
    {
        game.GameMenu();
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                game.Playing();
                file.WriteFile();
                break;
            case 2:
                file.ReadFile();
                break;
            case 3:
                Console.WriteLine("See you again!");
                break;
        }
    }
    catch (ArgumentException ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice < 3);