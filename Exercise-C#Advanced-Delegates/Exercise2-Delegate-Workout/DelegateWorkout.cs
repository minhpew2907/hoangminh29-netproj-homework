﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_C_Avanced_Generic_Delegate_Workout
{
    public abstract class DelegateWorkout<T>
    {
        protected delegate T MathOperation(T param1, T param2);
        protected virtual T PerformOperation(T param1, T param2, MathOperation operation)
        {
            return operation(param1, param2);
        }
    }
}
