﻿
using Exercise_C_Acvanced_Square_Cube;

SignatureManager signatureManager = new SignatureManager();

int choice = 0;
do
{
    try
    {
        Console.WriteLine("--------Custom Math--------");
        Console.WriteLine(" 1. Double.                ");
        Console.WriteLine(" 2. Triple.                ");
        Console.WriteLine(" 3. Square.                ");
        Console.WriteLine(" 4. Cube.                  ");
        Console.WriteLine(" 5. Exit.                  ");
        Console.WriteLine("---------------------------");
        Console.WriteLine("\nEnter your choice:       ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                signatureManager.DoubleNumber();
                break;
            case 2:
                signatureManager.TripleNumber();
                break;
            case 3:
                signatureManager.SquareNumber();
                break;
            case 4:
                signatureManager.CubeNumber();
                break;
            case 5:
                Console.WriteLine("Good Bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);